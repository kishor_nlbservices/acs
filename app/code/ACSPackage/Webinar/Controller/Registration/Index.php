<?php

/**
 * Created by PhpStorm.
 * User: sohelrana
 * Date: 9/16/17
 * Time: 4:47 PM
 */

namespace ACSPackage\Webinar\Controller\Registration;

use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Customer;

class Index extends \Magento\Framework\App\Action\Action {

    protected $customerSession;
    protected $customer;
    protected $encodeInterface;

    public function __construct(
            Context $context,
            Session $customerSession,
            Customer $customer,
            \Magento\Framework\Url\EncoderInterface $encodeInterface,
            \Magento\Framework\App\Response\Http $response
    ) {
        parent::__construct($context);
        $this->customerSession = $customerSession;
        $this->customer = $customer;
        $this->response = $response;
        $this->encodeInterface = $encodeInterface;
    }
    
    public function execute() {
        $meetingId = $this->getRequest()->getParam('id', 0);
        $sourceCode = $this->getRequest()->getParam('sc');
        
        if ($this->customerSession->isLoggedIn()) {
            $this->_view->loadLayout();
            $this->_view->renderLayout();
        } 
        else {
            if (empty($meetingId)) {
                $this->response->setRedirect('https://www.acs.org/');
            } else {
                $url = $this->_url->getUrl('webinar/registration/index', ['id' => $meetingId, 'sc' => $sourceCode]);
                
                $urlEncode = $this->encodeInterface->encode($url);
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath('sso/saml2/login', array('referer' => $urlEncode));
                
                return $resultRedirect;
            }
        }
    }
}
