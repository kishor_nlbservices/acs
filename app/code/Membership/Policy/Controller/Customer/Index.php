<?php
/**
 * Created by PhpStorm.
 * User: sohelrana
 * Date: 9/16/17
 * Time: 4:47 PM
 */

namespace Membership\Policy\Controller\Customer;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Customer;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $customerSession;

    protected $customers;

    public function __construct(
        Context $context,
        Session $customerSession,
        Customer $customers
    )
    {
        parent::__construct($context);
        $this->customerSession = $customerSession;
        $this->customers = $customers;       
    }

    public function execute()
    {  
      // echo "helloworld";exit;	
        if ($this->customerSession->isLoggedIn()) {

            //Get customer by customerID.
            $customerId = $this->customerSession->getCustomerData()->getId();
            $customer = $this->customers->load($customerId);
            
                $this->_view->loadLayout(); 
				//$this->_view->getLayout()->getBlock('page.main.title')->setPageTitle('Test TEst');
                $this->_view->renderLayout();  
                //$resultPage->getConfig()->getTitle()->set(__('Privacy Policy'));				
            
        }
        else{

            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('customer/account/login/');
            return $resultRedirect;        
        } 
    }
}   