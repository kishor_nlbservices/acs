<?php

namespace Webinar\WebsiteRestriction\Observer;

use Magento\Framework\Event\ObserverInterface;

class RestrictWebsite implements ObserverInterface {

    /**
     * RestrictWebsite constructor.
     */
    public function __construct(
            \Magento\Framework\App\Response\Http $response
    ) {
        $this->response = $response;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        $allowedRoutes = [
            'cms_index_index',
            'cms_page_view',
            'banner_ajax_load',
        ];

        $request = $observer->getEvent()->getRequest();
        $actionFullName = strtolower($request->getFullActionName());
        
//        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/website-restriction.log');
//        $logger = new \Zend\Log\Logger();
//        $logger->addWriter($writer);
//        $logger->info('===****=======');
//        $logger->info($actionFullName);
//        $logger->info($request->getParams());
        
        if (!in_array($actionFullName, $allowedRoutes)) {
            if ( (strpos($actionFullName, 'sso') === false) 
                    && (strpos($actionFullName, 'pitbulk') === false) 
                    && (strpos($actionFullName, 'webinar') === false) 
                    && (strpos($actionFullName, 'registration') === false) 
                    && (strpos($actionFullName, 'customer') === false) ) {
                $this->response->setRedirect('https://www.acs.org/');
            }
        }
    }

}
