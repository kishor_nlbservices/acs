<?php

namespace Webinar\Registration\Helper;

use Magento\Framework\App\Helper\Context;

class RealTime extends Config {

    public function __construct(
            Context $context,
            \Magento\Customer\Model\SessionFactory $customerSession
    ) {
        parent::__construct($context);
        $this->customerSession = $customerSession->create();
    }

    /**
     * @param string|boolean $requestData
     * @param array $headerData
     * @param array $url
     * @return void
     */
    public function curlPost($requestData, $headerData = [], $url, $constant = null) {
        if ($constant) {
            $result = $this->getComplexArray($constant);
            $headerData = array_merge($result, $headerData);
        }
        
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $requestData);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        
        if (!empty($headerData)) {
            curl_setopt(
                    $curl,
                    CURLOPT_HTTPHEADER,
                    $headerData
            );
        }
        
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        $result = curl_exec($curl);

        curl_close($curl);
        
        return $result;
    }

    /**
     * Undocumented function
     *
     * @param array $headerData
     * @param array $url
     * @return void
     */
    public function curlGet($headerData = [], $url, $constant = null) {
        if ($constant) {
            $result = $this->getComplexArray($constant);
            $headerData = array_merge($result, $headerData);
        }
        
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        
        if (!empty($headerData)) {
            curl_setopt(
                    $curl,
                    CURLOPT_HTTPHEADER,
                    $headerData
            );
        }
        
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        $result = curl_exec($curl);
        
        curl_close($curl);

        return $result;
    }

    /**
     * Get Auth key
     *
     * @return array
     */
    public function getAuth() {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/api.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info('----start getAuth()-----');
        
        $output = [];
        $url = $this->getConfig(Config::WEBINAR_MULESOFT_AUTH_URL);
        $logger->info($url);
        
        $result = $this->curlPost(false, [], $url);
        
        $logger->info($result);
                
        $resultData = json_decode($result, true);
        
        if (json_last_error() !== JSON_ERROR_NONE) {
            $output['error'] = 'Unable to unserialize value. Error: ' . json_last_error_msg();
        } 
        else if (!empty($resultData['error_description'])) {
            $output['error'] = $resultData['error_description'];
        } 
        else {
            $output = $resultData;
        }
        
        $logger->info($output);
        $logger->info('----end getAuth()-----\n\n');
        
        return $output;
    }

    /**
     * Undocumented function
     *
     * @param array $requestData
     * @param string $meetingId
     * @return string
     */
    public function updateCustomerData($requestData, $meetingId) {
        $output = [];
        $authData = $this->getAuth();

        if (!empty($authData) && isset($authData['access_token'])) {
            $url = $this->getConfig(Config::WEBINAR_MULESOFT_REGISTRANTS_URL);
            $url = $url . $meetingId;
            $headerData = ['Authorization:Bearer ' . $authData['access_token']];
            
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/api.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->info('----start updateCustomerData()-----');
            
            $logger->info($url);
            $logger->info($headerData);
            $logger->info($requestData);
            $logger->info(Config::WEBINAR_MULESOFT_REGISTRANTS);
            
            $result = $this->curlPost($requestData, $headerData, $url, Config::WEBINAR_MULESOFT_REGISTRANTS);
            
            $logger->info($result);
                        
            $resultData = json_decode($result, true);
            
            if (json_last_error() !== JSON_ERROR_NONE) {
                $output['error'] = 'Unable to unserialize value. Error: ' . json_last_error_msg();
                $output['response_data']=$resultData;
            } 
            else if ($resultData['success'] === false) {
                $output['error'] = !empty($resultData['result']['message']) ? $resultData['result']['message'] : $resultData['message'];
                $output['response_data']=$resultData;
            } 
            else {
                $output = $resultData;
            }
        } 
        else {
            $output['error'] = $authData['error'];
        }
        
        $logger->info($output);
        $logger->info('----end updateCustomerData()-----\n\n');
        
        return $output;
    }

    /**
     * Undocumented function
     *
     * @param array $requestData
     * @param string $meetingId
     * @return string
     */
    public function getCountry() {
        $output = [];
        $authData = $this->getAuth();
        
        if (!empty($authData) && isset($authData['access_token'])) {
            $url = $this->getConfig(Config::WEBINAR_MULESOFT_COUNTRY_URL);
            $headerData = ['Authorization:Bearer ' . $authData['access_token']];
            
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/api.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->info('----start getCountry()-----');
            
            $logger->info($url);
            $logger->info($headerData);
            $logger->info(Config::WEBINAR_MULESOFT_COUNTRY);
            
            $result = $this->curlGet($headerData, $url, Config::WEBINAR_MULESOFT_COUNTRY);
            
            $logger->info($result);
                        
            $resultData = json_decode($result, true);
            
            if (json_last_error() !== JSON_ERROR_NONE) {
                $output['error'] = 'Unable to unserialize value. Error: ' . json_last_error_msg();
            } 
            else if ($resultData['success'] === false) {
                $output['error'] = !empty($resultData['result']['message']) ? $resultData['result']['message'] : $resultData['message'];
            } 
            else {
                $output = $resultData;
            }
        } 
        else {
            $output['error'] = $authData['error'];
        }
        
        $logger->info($output);
        $logger->info('----end getCountry()-----\n\n');
        
        return $output;
    }
    
    /**
     * Undocumented function
     *
     * @param array $requestData
     * @param string $meetingId
     * @return string
     */
    public function getState() {
        $output = [];
        $authData = $this->getAuth();
        
        if (!empty($authData) && isset($authData['access_token'])) {
            $url = $this->getConfig(Config::WEBINAR_MULESOFT_STATE_URL);
            $headerData = ['Authorization:Bearer ' . $authData['access_token']];
            
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/api.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->info('----start getState()-----');
            
            $logger->info($url);
            $logger->info($headerData);
            $logger->info(Config::WEBINAR_MULESOFT_STATE);
            
            $result = $this->curlGet($headerData, $url, Config::WEBINAR_MULESOFT_STATE);
            
            $logger->info($result);
                        
            $resultData = json_decode($result, true);
            
            if (json_last_error() !== JSON_ERROR_NONE) {
                $output['error'] = 'Unable to unserialize value. Error: ' . json_last_error_msg();
            } 
            else if ($resultData['success'] === false) {
                $output['error'] = !empty($resultData['result']['message']) ? $resultData['result']['message'] : $resultData['message'];
            } 
            else {
                $output = $resultData;
            }
        } 
        else {
            $output['error'] = $authData['error'];
        }
        
        $logger->info($output);
        $logger->info('----end getState()-----\n\n');
        
        return $output;
    }
    
    public function netForumUpdate($requestData) {
        $output = [];
        $authData = $this->getAuth();
        
        if (!empty($authData) && isset($authData['access_token'])) {
            $url = $this->getConfig(Config::WEBINAR_MULESOFT_NET_FORM_URL);
            $headerData = ['Authorization:Bearer ' . $authData['access_token']];
            
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/api.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->info('----start netForumUpdate()-----');
            
            $logger->info($url);
            $logger->info($headerData);
            $logger->info($requestData);
            $logger->info(Config::WEBINAR_MULESOFT_NET_FORM);
            
            $result = $this->curlPost($requestData, $headerData, $url, Config::WEBINAR_MULESOFT_NET_FORM);
            
            $logger->info($result);
                        
            $resultData = json_decode($result, true);
            
            if (json_last_error() !== JSON_ERROR_NONE) {
                $output['error'] = 'Unable to unserialize value. Error: ' . json_last_error_msg();
                $output['response_data']=$result;
            } 
            else if ($resultData['success'] === false) {
                $output['error'] = !empty($resultData['result']['message']) ? $resultData['result']['message'] : $resultData['message'];
                $output['response_data']=$result;
            } 
            else {
                $output = $resultData;
            }
        } 
        else {
            $output['error'] = $authData['error'];
        }
        
        $logger->info($output);
        $logger->info('----end netForumUpdate()-----\n\n');
        
        return $output;
    }
    
    public function getWebinars($meetingId) {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/api.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info('----start getWebinars()-----');
        $webinarsDetails = $this->customerSession->getWebinarsDetails();
        
        if (isset($webinarsDetails[$meetingId])) {
            return $webinarsDetails[$meetingId];
        }
        
        $output = [];
        $authData = $this->getAuth();
        
        if (!empty($authData) && isset($authData['access_token'])) {
            $url = $this->getConfig(Config::WEBINAR_MULESOFT_WEBINARS_URL);
            $url = $url . $meetingId;
            $headerData = ['Authorization:Bearer ' . $authData['access_token']];
            
            
            $logger->info($url);
            $logger->info($headerData);
            $logger->info(Config::WEBINAR_MULESOFT_WEBINARS);
            
            $result = $this->curlGet($headerData, $url, Config::WEBINAR_MULESOFT_WEBINARS);
            
            $logger->info($result);
                        
            $resultData = json_decode($result, true);
            
            if (json_last_error() !== JSON_ERROR_NONE) {
                $output['error'] = 'Unable to unserialize value. Error: ' . json_last_error_msg();
            } 
            else if ($resultData['success'] === false) {
                $output['error'] = !empty($resultData['result']['message']) ? $resultData['result']['message'] : $resultData['message'];
            } 
            else {
                $output = [
                        'description' => $resultData['result']['webinarsResponse']['description'],
                        'name' => $resultData['result']['webinarsResponse']['name'],
                        'startTime' => $resultData['result']['webinarsResponse']['times'][0]['startTime'],
                        'endTime' => $resultData['result']['webinarsResponse']['times'][0]['endTime'],
                        ];
                
                $webinarsDetails = [$meetingId => $output];
                $this->customerSession->setWebinarsDetails($webinarsDetails);
            }
        } 
        else {
            $output['error'] = $authData['error'];
        }
        
        $logger->info($output);
        $logger->info('----end getWebinars()-----\n\n');
        
        return $output;
    }
}