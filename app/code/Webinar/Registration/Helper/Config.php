<?php
namespace Webinar\Registration\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;

class Config extends AbstractHelper
{
    const WEBINAR_MULESOFT_AUTH_URL="webinar/mulesoft/auth_url";
    const WEBINAR_MULESOFT_AUTH="webinar/mulesoft/auth";
    const WEBINAR_MULESOFT_REGISTRANTS_URL="webinar/mulesoft/registrants_url";
    const WEBINAR_MULESOFT_REGISTRANTS="webinar/mulesoft/registrants";
    const WEBINAR_MULESOFT_STATE="webinar/mulesoft/state";
    const WEBINAR_MULESOFT_STATE_URL="webinar/mulesoft/state_url";
    const WEBINAR_MULESOFT_COUNTRY="webinar/mulesoft/country";
    const WEBINAR_MULESOFT_COUNTRY_URL="webinar/mulesoft/country_url";
    const WEBINAR_MULESOFT_NET_FORM_URL="webinar/mulesoft/net_forum_url";
    const WEBINAR_MULESOFT_NET_FORM="webinar/mulesoft/net_forum";
    const WEBINAR_MULESOFT_WEBINARS_URL="webinar/mulesoft/webinars_url";
    const WEBINAR_MULESOFT_WEBINARS="webinar/mulesoft/webinars";
    const ACS_FOOTER_SOCIAL_FACEBOOK="acs_footer/social/facebook";
    const ACS_FOOTER_SOCIAL_TWITTER="acs_footer/social/twitter";
    const ACS_FOOTER_SOCIAL_LINKEDIN="acs_footer/social/linkedin";
    const ACS_FOOTER_FIRST_COLUMN="acs_footer/first/column";
    const ACS_FOOTER_SECOND_COLUMN="acs_footer/second/column";
    const ACS_FOOTER_THIRD_COLUMN="acs_footer/third/column";

    public function __construct(
        Context $context
    ) {
        parent::__construct($context);
    }

    public function getConfig($config_path)
    {
        return $this->scopeConfig->getValue(
            $config_path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getComplexArray($constant,$api=true)
    {
        $result=$this->getConfig($constant);
        $output=[];
        if ($result) {
            $result=json_decode($result, true);
            foreach ($result as $key => $value) {
                if($api){
                    $output[]=$value['key'].':'.$value['value'];
                }else{
                    $output[$value['key']]=$value['value'];
                }
            }
        }
        return $output;
    }
}
