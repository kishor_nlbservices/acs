<?php
namespace Webinar\Registration\Helper;

use Magento\Framework\App\Helper\Context;

class Data extends Config
{
    protected $customerSession;
    protected $addressFactory;

    /**
     * Undocumented function
     *
     * @param Context $context
     * @param \Magento\Customer\Model\SessionFactory $customerSession
     * @param \Magento\Customer\Model\AddressFactory $addressFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Catalog\Model\Product $product
     * @param \Magento\Framework\Data\Form\FormKey $formkey
     * @param \Magento\Quote\Model\QuoteFactory $quote
     * @param \Magento\Quote\Model\QuoteManagement $quoteManagement
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
     * @param \Magento\Sales\Model\Service\OrderService $orderService
     * @param \Webinar\Registration\Model\ResourceModel\Country\CollectionFactory $countryCollectionFactory
     * @param \Webinar\Registration\Model\ResourceModel\Region\CollectionFactory $regionCollectionFactory
     */
    public function __construct(
        Context $context,
        \Magento\Customer\Model\SessionFactory $customerSession,
        \Magento\Customer\Model\AddressFactory $addressFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\Product $product,
        \Magento\Framework\Data\Form\FormKey $formkey,
        \Magento\Quote\Model\QuoteFactory $quote,
        \Magento\Quote\Model\QuoteManagement $quoteManagement,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Sales\Model\Service\OrderService $orderService,
        \Webinar\Registration\Model\ResourceModel\Country\CollectionFactory $countryCollectionFactory,
        \Webinar\Registration\Model\ResourceModel\Region\CollectionFactory $regionCollectionFactory
    ) {
        parent::__construct($context);
        $this->customerSession = $customerSession->create();
        $this->addressFactory = $addressFactory;
        $this->_storeManager = $storeManager;
        $this->_product = $product;
        $this->_formkey = $formkey;
        $this->quote = $quote;
        $this->quoteManagement = $quoteManagement;
        $this->customerFactory = $customerFactory;
        $this->customerRepository = $customerRepository;
        $this->orderService = $orderService;
        $this->countryCollectionFactory = $countryCollectionFactory;
        $this->regionCollectionFactory = $regionCollectionFactory;
    }
    /**
     * Undocumented function
     *
     * @param string $requestData
     * @param array $headerData
     * @param array $url
     * @return void
     */
    public function createCustomerAddress($data, $updateCustomer=false)
    {
        $customerId= $this->customerSession->getCustomer()->getId();
        $defaultBilling= $this->customerSession->getCustomer()->getDefaultBilling();
        if ($customerId) {
            $address = $this->addressFactory->create();
            //------update customer-----
            if ($updateCustomer) {
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $approverModel = $objectManager->get('\Magento\Customer\Api\CustomerRepositoryInterface');
                $customer = $approverModel->getById($customerId);
                
                if (!empty($data['startDate'])) {
                    $customer->setCustomAttribute('start_date', $data['startDate']);
                    $customer->setCustomAttribute('end_date', $data['endDate']);
                }
                
                $customer->setCustomAttribute('membertype', $data['membertype']);
                $customer->setCustomAttribute('member_api_exception', $data['member_api_exception']);
                $customer->setCustomAttribute('member_api_exception_message', $data['member_api_exception_message']);
                
                $approverModel->save($customer);
            }


            
        
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/kishor-customer.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->info('=======');
            $logger->info($customerId);
            $logger->info($data);
        
        

            //------update customer-----
            $address->setCustomerId($customerId)
                        ->setFirstname($data['firstname'])
                        ->setLastname($data['lastname'])
                        ->setCountryId($data['country_id'])
                        ->setRegionId($data['region_id'])
                        ->setPostcode($data['postcode'])
                        //->setTelephone($data['telephone'])
                        ->setCompany($data['company'])
                        ->setCity('.')
                        ->setStreet('.')
                        ->setIsDefaultBilling('1')
                        ->setIsDefaultShipping('1')
                        ->setSaveInAddressBook('1');
            if ($defaultBilling) {
                $address->setId($defaultBilling);
            }
            $address->save();
        }
    }

    /**
     * get login customer
     * billing address
     *
     * @return array|null
     */
    public function getDefaultBillingAddress()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->create('Magento\Customer\Model\Session');
        $address=[];
        $defaultBilling= $customerSession->getCustomer()->getDefaultBilling();
        if ($defaultBilling) {
            $address = $this->addressFactory->create()->load($defaultBilling)->getData();
        }
        return  $address;
    }

    public function getCustomerSessionData()
    {
        $customerID = $this->customerSession->getCustomer()->getId(); // your customer-id
        $customerObj = $this->customerFactory->create()->load($customerID);
        $customerFirstName = $customerObj->getData();
        return $customerFirstName;
    }

    public function getFreeRegistrationMessage()
    {
        if ($this->customerSession->getNetForumRegister()) {
            $this->customerSession->unsNetForumRegister();
            return true;
        }
        return null;
    }
    
    /**
     * Create Order for Package A
     *
     * @return array
     *
    */
    public function createOrderPackageA()
    {
        $customerData=$this->getCustomerSessionData();
        $defaultBilling= $customerData['default_billing'];
        $address = $this->addressFactory->create()->load($defaultBilling)->getData();
        $productId=$this->_product->getIdBySku('P-A0');

        if ($productId) {
            $orderData=[
                'currency_id'  => 'USD',
                'email'        =>  $customerData['email'], //buyer email id
                'shipping_address' =>$address,
              'items'=> [
                         ['product_id'=>$productId,'qty'=>1,'price'=>0]
                       ]
           ];
            $store=$this->_storeManager->getStore();
            $websiteId = $this->_storeManager->getStore()->getWebsiteId();
            $customer=$this->customerFactory->create();
    
            $customer->setWebsiteId($websiteId);
            $customer->loadByEmail($orderData['email']);// load customet by email address
            if (!$customer->getEntityId()) {
                //If not avilable then create this customer
                $customer->setWebsiteId($websiteId)
                        ->setStore($store)
                        ->setFirstname($orderData['shipping_address']['firstname'])
                        ->setLastname($orderData['shipping_address']['lastname'])
                        ->setEmail($orderData['email'])
                        ->setPassword($orderData['email']);
                $customer->save();
            }
            $quote=$this->quote->create(); //Create object of quote
            $quote->setStore($store); //set store for which you create quote
            // if you have allready buyer id then you can load customer directly
            $customer= $this->customerRepository->getById($customer->getEntityId());
            $quote->setCurrency();
            $quote->assignCustomer($customer); //Assign quote to customer
    
            //add items in quote
            foreach ($orderData['items'] as $item) {
                $product=$this->_product->load($item['product_id']);
                $product->setPrice($item['price']);
                $quote->addProduct(
                    $product,
                    intval($item['qty'])
                );
            }
    
            //Set Address to quote
            $quote->getBillingAddress()->addData($orderData['shipping_address']);
            $quote->getShippingAddress()->addData($orderData['shipping_address']);
    
            // Collect Rates and Set Shipping & Payment Method
    
            $shippingAddress=$quote->getShippingAddress();
            $shippingAddress->setCollectShippingRates(true)
                            ->collectShippingRates()
                            ->setShippingMethod('freeshipping_freeshipping'); //shipping method
           //$quote->setPaymentMethod('checkmo'); //payment method
            //$quote->setInventoryProcessed(false); //not effetc inventory
            $quote->save(); //Now Save quote and your quote is ready
    
            // Set Sales Order Payment
            $quote->getPayment()->importData(['method' => 'checkmo']);
    
            // Collect Totals & Save Quote
            $quote->collectTotals()->save();
            // Create Order From Quote
            $order = $this->quoteManagement->submit($quote);
            $order->setEmailSent(0);
            $increment_id = $order->getRealOrderId();
            if ($order->getEntityId()) {
                $result['order_id']= $order->getRealOrderId();
            } else {
                $result=['error'=>1,'msg'=>'order not placed'];
            }
        } else {
            $result=['error'=>1,'msg'=>"product doesn't exist"];
        }
        
        return $result;
    }

    public function getWebinarCountry($countryCode=null)
    {
        $countryCollectionFactory =$this->countryCollectionFactory->create();
        if ($countryCode) {
            $countryCollectionFactory->addFieldToFilter('mage_country_id', ['eq' => $countryCode]);
        }
        return $countryCollectionFactory->getData();
    }

    public function getWebinarRegions($countryCode=null, $regionId=null)
    {
        $regionCollectionFactory = $this->regionCollectionFactory->create();
        if ($countryCode) {
            $regionCollectionFactory->addFieldToFilter('mage_country_id', ['eq' => $countryCode]);
        }
        if ($regionId) {
            $regionCollectionFactory->addFieldToFilter('mage_region_id', ['eq' => $regionId]);
        }
        return $regionCollectionFactory->getData();
    }

    public function getWebinarRegionJson()
    {
        $region=$this->getWebinarRegions();
        $output=[];
        foreach ($region as $key => $value) {
            $output[$value['mage_country_id']][]=$value;
        }

        return json_encode($output);
    }

    public function getFooterData($type, $location=null)
    {
        $output=null;
        if ($type=='column') {
            $output=[];
            if ($location=='first') {
                $output=$this->getComplexArray(Config::ACS_FOOTER_FIRST_COLUMN, false);
            }
            if ($location=='second') {
                $output=$this->getComplexArray(Config::ACS_FOOTER_SECOND_COLUMN, false);
            }
            if ($location=='third') {
                $output=$this->getComplexArray(Config::ACS_FOOTER_THIRD_COLUMN, false);
            }
            /* echo "<pre>";
            print_r($output);
            die(); */
        }
        if ($type=='link') {
            if ($location=='facebook') {
                $output=$this->getConfig(Config::ACS_FOOTER_SOCIAL_FACEBOOK);
            }
            if ($location=='twitter') {
                $output=$this->getConfig(Config::ACS_FOOTER_SOCIAL_TWITTER);
            }
            if ($location=='linkedin') {
                $output=$this->getConfig(Config::ACS_FOOTER_SOCIAL_LINKEDIN);
            }
        }
        return $output;
    }
}
