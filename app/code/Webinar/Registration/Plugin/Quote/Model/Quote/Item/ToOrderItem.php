<?php
namespace Webinar\Registration\Plugin\Quote\Model\Quote\Item;

use Magento\Quote\Model\Quote\Item\ToOrderItem as QuoteToOrderItem;
use Magento\Framework\Serialize\Serializer\Json;

class ToOrderItem
{
    public function __construct(
        Json $serializer = null,
        \Magento\Catalog\Model\Product $productObject,
        \Packagecreate\Package\Model\Config\Product\Listofproducts $listOfProductObject
    ) {
        $this->listOfProductObject=$listOfProductObject;
        $this->productObject=$productObject;
        $this->serializer = $serializer ?: \Magento\Framework\App\ObjectManager::getInstance()
            ->get(\Magento\Framework\Serialize\Serializer\Json::class);
    }

    public function aroundConvert(
        QuoteToOrderItem $subject,
        \Closure $proceed,
        $item,
        $data = []
    ) {
        $orderItem = $proceed($item, $data);
        //--------------
        $sku=$orderItem->getSku();
        if ($sku=='P-A0') {
            $product = $this->productObject->loadByAttribute('sku', $sku);
            $productIds=$product->getData('product_ids');
            if ($productIds) {
                $attributeOptionAll = $this->listOfProductObject->getAllOptions();
                $productIds=explode(',', $productIds);
                $productAttr=[];
                foreach ($attributeOptionAll as $key => $value) {
                    if (in_array($value['value'], $productIds)) {
                        $productAttr[$value['value']]=$value['label'];
                    }
                }
                $orderItem->setProductsIds($this->serializer->serialize($productAttr));
            }
        }
        //--------------
        return $orderItem;
    }
}
