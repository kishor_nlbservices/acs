<?php
namespace Webinar\Registration\Api\Data;

interface WebinarInterface
{
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case.
     */
    const ID = 'id';
    const CUSTOMER_ID = 'customer_id';
    const MEETING_ID = 'meeting_id';
    const NAME = 'name';
    const END_TIME = 'end_time';
    const START_TIME = 'start_time';
    const DESCRIPTION = 'description';
    const CREATED_AT = 'created_at';
    const EXCEPTION = 'exception';

    /**
     * Get EntityId.
     *
     * @return int
     */
    public function getId();

    /**
     * Set EntityId.
     */
    public function setId($id);


    /**
     * Get Customer Id.
     *
     * @return int
     */
    public function getCustomerId();

    /**
     * Set Customer Id.
     */
    public function setCustomerId($customerId);

    /**
     * Get StartTime.
     *
     * @return string|null
     */
    public function getStartTime();

    /**
     * Set StartTime.
     */
    public function setStartTime($startTime);

    /**
     * Get Description.
     *
     * @return string|null
     */
    public function getDescription();

    /**
     * Set Description.
     */
    public function setDescription($description);

    /**
     * Get Description.
     *
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set Description.
     */
    public function setCreatedAt($createdAt);

    /**
     * Get Exception.
     *
     * @return string|null
     */
    public function getException();

    /**
     * Set Exception.
     */
    public function setException($exception);

    /**
     * Get MeetingId.
     *
     * @return varchar
     */
    public function getMeetingId();

    /**
     * Set MeetingId.
     */
    public function setMeetingId($meetingId);

    /**
     * Get name.
     *
     * @return string
     */
    public function getName();

    /**
     * Set name.
     */
    public function setName($name);

    /**
     * Get EndTime.
     *
     * @return string|null
     */
    public function getEndTime();

    /**
     * Set EndTime.
     */
    public function setEndTime($endTime);
}
