<?php
namespace Webinar\Registration\Api\Data;

interface ErrorReportInterface
{
    const ID = 'id';
    const CUSTOMER_ID = 'customer_id';

    /**
     * Get EntityId.
     *
     * @return int
     */
    public function getId();

    /**
     * Set EntityId.
     */
    public function setId($id);


    /**
     * Get Customer Id.
     *
     * @return int
     */
    public function getCustomerId();

    /**
     * Set Customer Id.
     */
    public function setCustomerId($customerId);
}
