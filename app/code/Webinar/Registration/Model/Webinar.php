<?php
namespace Webinar\Registration\Model;

use Webinar\Registration\Api\Data\WebinarInterface;

class Webinar extends \Magento\Framework\Model\AbstractModel implements WebinarInterface
{
    /**
     * CMS page cache tag.
     */
    const CACHE_TAG = 'acs_webinars_details';

    /**
     * @var string
     */
    protected $_cacheTag = 'acs_webinars_details';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'acs_webinars_details';

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('Webinar\Registration\Model\ResourceModel\Webinar');
    }
    /**
     * Get Id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * Set Id.
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }
    
    /**
     * Get Customer Id.
     *
     * @return int
     */
    public function getCustomerId()
    {
        return $this->getData(self::CUSTOMER_ID);
    }

    /**
     * Set Customer Id.
     */
    public function setCustomerId($customerId)
    {
        return $this->setData(self::CUSTOMER_ID, $customerId);
    }

    /**
     * Get MeetingId.
     *
     * @return varchar
     */
    public function getMeetingId()
    {
        return $this->getData(self::MEETING_ID);
    }

    /**
     * Set MeetingId.
     */
    public function setMeetingId($meetingId)
    {
        return $this->setData(self::MEETING_ID, $meetingId);
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->getData(self::NAME);
    }

    /**
     * Set name.
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * Get EndTime.
     *
     * @return string|null
     */
    public function getEndTime()
    {
        return $this->getData(self::END_TIME);
    }

    /**
     * Set EndTime.
     */
    public function setEndTime($endTime)
    {
        return $this->setData(self::END_TIME, $endTime);
    }
    
    /**
     * Get StartTime.
     *
     * @return string|null
     */
    public function getStartTime()
    {
        return $this->getData(self::START_TIME);
    }

    /**
     * Set StartTime.
     */
    public function setStartTime($startTime)
    {
        return $this->setData(self::START_TIME, $startTime);
    }
    
    /**
     * Get Description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->getData(self::DESCRIPTION);
    }

    /**
     * Set Description.
     */
    public function setDescription($description)
    {
        return $this->setData(self::DESCRIPTION, $description);
    }
    
    /**
     * Get Description.
     *
     * @return string|null
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * Set Description.
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }
    
    /**
     * Get Exception.
     *
     * @return string|null
     */
    public function getException()
    {
        return $this->getData(self::EXCEPTION);
    }

    /**
     * Set Exception.
     */
    public function setException($exception)
    {
        return $this->setData(self::EXCEPTION, $exception);
    }
}
