<?php
namespace Webinar\Registration\Model;

class Region extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Webinar\Registration\Model\ResourceModel\Region');
    }
}
