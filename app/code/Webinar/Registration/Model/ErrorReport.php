<?php
namespace Webinar\Registration\Model;

use Webinar\Registration\Api\Data\ErrorReportInterface;

class ErrorReport extends \Magento\Framework\Model\AbstractModel implements ErrorReportInterface
{
    /**
     * CMS page cache tag.
     */
    const CACHE_TAG = 'acs_error_report';

    /**
     * @var string
     */
    protected $_cacheTag = 'acs_error_report';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'acs_error_report';

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('Webinar\Registration\Model\ResourceModel\ErrorReport');
    }
    /**
     * Get Id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * Set Id.
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }
    
    /**
     * Get Customer Id.
     *
     * @return int
     */
    public function getCustomerId()
    {
        return $this->getData(self::CUSTOMER_ID);
    }

    /**
     * Set Customer Id.
     */
    public function setCustomerId($customerId)
    {
        return $this->setData(self::CUSTOMER_ID, $customerId);
    }
}
