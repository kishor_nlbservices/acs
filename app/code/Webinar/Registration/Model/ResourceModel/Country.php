<?php
namespace Webinar\Registration\Model\ResourceModel;

class Country extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('acs_directory_country', 'id');
    }
}
