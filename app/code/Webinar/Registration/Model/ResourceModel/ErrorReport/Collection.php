<?php
namespace Webinar\Registration\Model\ResourceModel\ErrorReport;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Webinar\Registration\Model\Webinar', 'Webinar\Registration\Model\ResourceModel\ErrorReport');
        $this->_map['fields']['id'] = 'acs_error_report.id';
    }

    /**
     * Prepare page's statuses.
     * Available event cms_page_get_available_statuses to customize statuses.
     *
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Disabled')];
    }

    /**
	 * Join table `customer_grid_flat` with `acs_error_report`
	 *
	 * @return Collection
	 */
	public function addCustomerData() {            
        $this->getSelect()->joinLeft(
            ['join_table' => $this->getTable('customer_entity')],
            'main_table.customer_id = join_table.entity_id',
            ['firstname','lastname','email']
        );
		return $this;
	}
}
