<?php

namespace Webinar\Registration\Model\ResourceModel\Webinar\Grid;

use Magento\Framework\Search\AggregationInterface;
use Magento\Framework\Api\Search\SearchResultInterface;
use Magento\Framework\View\Element\UiComponent\DataProvider\Document;
use Magento\Framework\Api\SearchCriteriaInterface;
use Webinar\Registration\Model\ResourceModel\Webinar;
use Webinar\Registration\Model\ResourceModel\Webinar\Collection as GridCollection;

class Collection extends GridCollection implements SearchResultInterface
{
    protected $aggregations;

    protected function _construct()
    {
        $this->_init(Document::class, Webinar::class);
    }


    protected function _initSelect()
    {
        parent::_initSelect();

        //$this->addFilterToMap('category_uid', 'join_category.entity_id');

        $this->getSelect()->joinLeft(
            ['join_table' => $this->getTable('customer_grid_flat')],
            'main_table.customer_id = join_table.entity_id',
            ['billing_country_id','billing_region','billing_company','constituentid','membertype','ismember','end_date','start_date','email']
        );
    }
    
    public function setItems(array $items = null)
    {
        return $this;
    }

    public function getSearchCriteria()
    {
        return null;
    }
    
    public function getAggregations()
    {
        return $this->aggregations;
    }

    public function setAggregations($aggregations)
    {
        $this->aggregations = $aggregations;
    }

    public function setSearchCriteria(SearchCriteriaInterface $searchCriteria = null)
    {
        return $this;
    }

    public function getTotalCount()
    {
        return $this->getSize();
    }

    public function setTotalCount($totalCount)
    {
        return $this;
    }
    
    /*

    public function getAllIds($limit = null, $offset = null) {
        return $this->getConnection()->fetchCol($this->_getAllIdsSelect($limit, $offset), $this->_bindParams);
    }

    */
}
