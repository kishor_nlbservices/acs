<?php
namespace Webinar\Registration\Model\ResourceModel;

class Region extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('acs_directory_country_region', 'id');
    }
}
