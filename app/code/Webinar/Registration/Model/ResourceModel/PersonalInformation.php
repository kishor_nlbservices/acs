<?php
namespace Webinar\Registration\Model\ResourceModel;

class PersonalInformation extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('acs_customer_personal_information', 'id');
    }
}
