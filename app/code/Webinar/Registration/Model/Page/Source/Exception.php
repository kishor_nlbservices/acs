<?php
namespace Webinar\Registration\Model\Page\Source;

use Magento\Framework\Data\OptionSourceInterface;

class Exception implements OptionSourceInterface
{
    /**
     *
     * @var \Magento\Cms\Model\Page
     */
    public $cmsPage;
    
    /**
     * Constructor
     *
     * @param \Magento\Cms\Model\Page $cmsPage
     */
    public function __construct(\Magento\Cms\Model\Page $cmsPage)
    {
        $this->cmsPage = $cmsPage;
    }
    
    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options =  [];
        
        $options [] =  [
                'label' => 'Success',
                'value' => 0
        ];
        $options [] =  [
                'label' => 'Failed',
                'value' => 1
        ];
        
        return $options;
    }
}
