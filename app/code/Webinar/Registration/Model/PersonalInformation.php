<?php
namespace Webinar\Registration\Model;

class PersonalInformation extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Webinar\Registration\Model\ResourceModel\PersonalInformation');
    }
}
