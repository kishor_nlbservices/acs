<?php

namespace Webinar\Registration\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();
        
        if (version_compare($context->getVersion(), '1.0.18', '<')) {
            $installer->getConnection()->addColumn(
                $installer->getTable('acs_webinars_details'),
                'exception_message',
                [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' => 'exception message',
                        'length' => 255,
                    ]
            );
        }
        if (version_compare($context->getVersion(), '1.0.19', '<')) {
            if ($tableName = $installer->getTable('acs_webinars_details')) {
                $columns = [
                        'firstname' => [
                                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                                'nullable' => false,
                                'length' => 100,
                                'comment' => 'First Name',
                        ],
                        'lastname' => [
                                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                                'nullable' => false,
                                'length' => 100,
                                'comment' => 'Last Name',
                            ],
                        'company' => [
                                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                                'nullable' => false,
                                'length' => 150,
                                'comment' => 'Company/Affiliate Type',
                            ],
                        'work' => [
                                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                                'nullable' => false,
                                'length' => 150,
                                'comment' => 'Work Function',
                            ],
                        'secondary_education' =>  [
                                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                                'nullable' => false,
                                'length' => 150,
                                'comment' => 'Post - Secondary Education Completed',
                            ],
                        'age' => [
                                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                                'nullable' => false,
                                'length' => 100,
                                'comment' => 'Age',
                            ],
                        'how_familiar' => [
                                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                                'nullable' => false,
                                'length' => 150,
                                'comment' => 'How familiar are you with the Webinar Topic?',
                            ],
                        'would_you_like' => [
                            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            'nullable' => false,
                            'length' => 25,
                            'comment' => 'Would you like to find out more about and hear from the co-producer of this broadcast?',
                        ],
                        'comments' => [
                                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                                'nullable' => false,
                                'length' => '100k',
                                'comment' => 'Questions & Comments',
                            ]

                    ];
                    
                $connection = $installer->getConnection();
                foreach ($columns as $name => $definition) {
                    $connection->addColumn($tableName, $name, $definition);
                }
            }
            if (!$installer->tableExists('acs_error_report')) {
                $acsErrorReport = $installer->getTable('acs_error_report');

                $table = $installer->getConnection()->newTable($acsErrorReport)
                        ->addColumn(
                            'id',
                            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                            null,
                            [
                                    'identity' => true,
                                    'nullable' => false,
                                    'primary' => true,
                                    'unsigned' => true,
                                ],
                            'ID'
                        )
                        ->addColumn(
                            'error_type',
                            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            255,
                            ['nullable => false'],
                            'Error report'
                        )
                        ->addColumn(
                            'meeting_id',
                            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            100,
                            ['nullable => true'],
                            'Meeting Id'
                        )
                        ->addColumn(
                            'error_massage',
                            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            255,
                            ['nullable' => true],
                            'Error Message'
                        )
                        ->addColumn(
                            'request_data',
                            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            '85k',
                            ['nullable' => false],
                            'request data'
                        )
                        ->addColumn(
                            'response',
                            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            '85k',
                            ['nullable' => false],
                            'response data'
                        )
                        ->addColumn(
                            'customer_id',
                            Table::TYPE_INTEGER,
                            null,
                            ['unsigned' => true, 'nullable' => false],
                            'customer Id'
                        )
                        ->addColumn(
                            'webinar_id',
                            Table::TYPE_INTEGER,
                            null,
                            ['unsigned' => true, 'nullable' => true],
                            'webinar Id'
                        )
                        ->addForeignKey(
                            $installer->getFkName(
                                $acsErrorReport,
                                'webinar_id',
                                $installer->getTable('acs_webinars_details'),
                                'id'
                            ),
                            'webinar_id',
                            $installer->getTable('acs_webinars_details'),
                            'id',
                            Table::ACTION_CASCADE
                        )
                        ->addForeignKey(
                            $installer->getFkName(
                                $acsErrorReport,
                                'customer_id',
                                $installer->getTable('customer_entity'),
                                'entity_id'
                            ),
                            'customer_id',
                            $installer->getTable('customer_entity'),
                            'entity_id',
                            Table::ACTION_CASCADE
                        )
                        ->setComment('webinar error report');
                $installer->getConnection()->createTable($table);
            }
        }        
        if (version_compare($context->getVersion(), '1.0.20', '<')) {
            if ($tableName = $installer->getTable('acs_error_report')) {
                $columns = [
                                'created_at' => [
                                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                                        'nullable' => false,
                                        'length' => 100,
                                        'comment' => 'Created At',
                                        'default' => Table::TIMESTAMP_INIT
                                ]
                            ];
                $connection = $installer->getConnection();
                foreach ($columns as $name => $definition) {
                    $connection->addColumn($tableName, $name, $definition);
                }
            }
        }
        
        if (version_compare($context->getVersion(), '1.0.17', '<')) {
            $tableName = $installer->getTable('acs_customer_personal_information');
            $installer->getConnection()->addColumn(
                $tableName,
                'acs_webinars_id',
                [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        'nullable' => false,
                        'unsigned' => true,
                        'comment' => 'webinars id',
                    ]
            );


            $setup->getConnection()->addForeignKey(
                $setup->getFkName(
                    'acs_customer_personal_information',
                    'acs_webinars_id',
                    $setup->getTable('acs_webinars_details'),
                    'id'
                ),
                $tableName,
                'acs_webinars_id',
                $setup->getTable('acs_webinars_details'),
                'id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            );

            $installer->getConnection()->changeColumn(
                $tableName,
                'customer_id',
                'customer_id',
                [
                        'type' => Table::TYPE_INTEGER,
                        'nullable' => false,
                        'unsigned' => true,
                        'comment' => 'Customer Id',
                    ]
            );

            $installer->getConnection()
                    ->addForeignKey(
                        $installer->getFkName(
                            $tableName,
                            'customer_id',
                            $installer->getTable('customer_entity'),
                            'entity_id'
                        ),
                        $tableName,
                        'customer_id',
                        $installer->getTable('customer_entity'),
                        'entity_id',
                        \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
                    );
        }

        if (version_compare($context->getVersion(), '1.0.16', '<')) {
            $installer->getConnection()->addColumn(
                $installer->getTable('acs_webinars_details'),
                'source_code',
                [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' => 'source code from acs',
                        'length' => 255,
                    ]
            );
        }

        if (version_compare($context->getVersion(), '1.0.15', '<')) {
            $installer->getConnection()->addColumn(
                $installer->getTable('acs_webinars_details'),
                'exception',
                [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                        'nullable' => false,
                        'comment' => 'exception while webinars create',
                        'default' => 0,
                    ]
            );
        }

        if (version_compare($context->getVersion(), '1.0.14', '<')) {
            $query = "INSERT INTO `core_config_data` (`scope`, `scope_id`, `path`, `value`) VALUES
            ('default', 0, 'acs_footer/third/column', '{\"_1583494625822_822\":{\"key\":\"Donate\",\"value\":\"https:\\/\\/www.acs.org\\/content\\/acs\\/en\\/donate.html?sc=180808_GlobalFooter_od\"},\"_1583494638890_890\":{\"key\":\"Volunteer\",\"value\":\"https:\\/\\/www.acs.org\\/content\\/acs\\/en\\/volunteer.html?sc=180808_GlobalFooter_od\"},\"_1583494644610_610\":{\"key\":\"ACS Network\",\"value\":\"https:\\/\\/communities.acs.org\\/welcome?sc=180808_GlobalFooter_od\"}}'),
            ('default', 0, 'acs_footer/second/column', '{\"_1583494479611_611\":{\"key\":\"Join ACS\",\"value\":\"https:\\/\\/join.acs.org\\/eweb\\/ACSOMATemplate.aspx?site=ACS_OMA&WebCode=CreateAccount&sc=190101_GlobalFooter_od\"},\"_1583494496257_257\":{\"key\":\"Renew Membership\",\"value\":\"http:\\/\\/www.renew.acs.org\\/?sc=190101_GlobalFooter_od\"},\"_1583494501305_305\":{\"key\":\"Member Benefits\",\"value\":\"https:\\/\\/www.acs.org\\/content\\/acs\\/en\\/membership\\/member-benefits.html?sc=190101_GlobalFooter_od\"}}'),
            ('default', 0, 'acs_footer/first/column', '{\"_1583494337265_265\":{\"key\":\"About ACS\",\"value\":\"https:\\/\\/www.acs.org\\/content\\/acs\\/en\\/about.html?sc=180808_GlobalFooter_od\"},\"_1583494414818_818\":{\"key\":\"Press Room\",\"value\":\"https:\\/\\/www.acs.org\\/content\\/acs\\/en\\/pressroom\\/news-room.html?sc=180808_GlobalFooter_od\"},\"_1583494419881_881\":{\"key\":\"Jobs at ACS\",\"value\":\"https:\\/\\/jobs.acs.org\\/?utm_source=careersite\"},\"_1583494425297_297\":{\"key\":\"Governance\",\"value\":\"https:\\/\\/www.acs.org\\/content\\/acs\\/en\\/about\\/governance.html?sc=180808_GlobalFooter_od\"},\"_1583494430721_721\":{\"key\":\"ACS Store\",\"value\":\"https:\\/\\/www.store.acs.org\\/?sc=180808_GlobalFooter_od\"}}');";
            $installer->run($query);
        }

        if (version_compare($context->getVersion(), '1.0.11', '<')) {
            $eavTable = $installer->getTable('sales_order_item');
            $columns = [
                'products_ids' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'selected item of products',
                    'length' => '64k',
                ],
            ];

            $connection = $installer->getConnection();
            foreach ($columns as $name => $definition) {
                $connection->addColumn($eavTable, $name, $definition);
            }
        }

        if (version_compare($context->getVersion(), '1.0.13', '<')) {
            if (!$installer->tableExists('acs_directory_country')) {
                $acsDirectoryCountry = $installer->getTable('acs_directory_country');

                $table = $installer->getConnection()->newTable($acsDirectoryCountry)
                        ->addColumn(
                            'id',
                            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                            null,
                            [
                                    'identity' => true,
                                    'nullable' => false,
                                    'primary' => true,
                                    'unsigned' => true,
                                ],
                            'ID'
                        )
                        ->addColumn(
                            'country_id',
                            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            255,
                            ['nullable => false'],
                            'webinar country code'
                        )
                        ->addColumn(
                            'country_name',
                            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            255,
                            ['nullable => false'],
                            'webinar country name'
                        )
                        ->addColumn(
                            'mage_country_id',
                            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            255,
                            ['nullable => false'],
                            'magento country code'
                        )
                        ->setComment('webinar country map with magento');
                $installer->getConnection()->createTable($table);
            }

            if (!$installer->tableExists('acs_directory_country_region')) {
                $acsDirectoryCountryRegion = $installer->getTable('acs_directory_country_region');

                $table = $installer->getConnection()->newTable($acsDirectoryCountryRegion)
                        ->addColumn(
                            'id',
                            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                            null,
                            [
                                    'identity' => true,
                                    'nullable' => false,
                                    'primary' => true,
                                    'unsigned' => true,
                                ],
                            'ID'
                        )
                        ->addColumn(
                            'code',
                            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            255,
                            ['nullable => false'],
                            'webinar region code'
                        )
                        ->addColumn(
                            'name',
                            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            255,
                            ['nullable => false'],
                            'webinar region name'
                        )
                        ->addColumn(
                            'country_id',
                            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                            null,
                            ['unsigned' => true, 'nullable' => false],
                            'webinar country id'
                        )
                        ->addColumn(
                            'mage_country_id',
                            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            3,
                            ['unsigned' => true, 'nullable' => false],
                            'magento country id'
                        )
                        ->addColumn(
                            'mage_region_id',
                            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                            null,
                            ['unsigned' => true, 'nullable' => false],
                            'magento region id'
                        )
                        ->addForeignKey(
                            $installer->getFkName(
                                $acsDirectoryCountryRegion,
                                'mage_region_id',
                                $installer->getTable('directory_country_region'),
                                'region_id'
                            ),
                            'mage_region_id',
                            $installer->getTable('directory_country_region'),
                            'region_id',
                            Table::ACTION_CASCADE
                        )
                        ->addForeignKey(
                            $installer->getFkName(
                                $acsDirectoryCountryRegion,
                                'country_id',
                                $installer->getTable('acs_directory_country'),
                                'id'
                            ),
                            'country_id',
                            $installer->getTable('acs_directory_country'),
                            'id',
                            Table::ACTION_CASCADE
                        )
                        ->setComment('webinar region map with magento');
                $installer->getConnection()->createTable($table);
            }
        }

        if (version_compare($context->getVersion(), '1.0.12', '<')) {
            if (!$installer->tableExists('acs_webinars_details')) {
                $acsCustomerPersonalTable = $installer->getTable('acs_webinars_details');

                $table = $installer->getConnection()->newTable($acsCustomerPersonalTable)
                        ->addColumn(
                            'id',
                            Table::TYPE_INTEGER,
                            null,
                            [
                                    'identity' => true,
                                    'nullable' => false,
                                    'primary' => true,
                                    'unsigned' => true,
                                ],
                            'ID'
                        )
                        ->addColumn(
                            'customer_id',
                            Table::TYPE_INTEGER,
                            null,
                            ['unsigned' => true, 'nullable' => false],
                            'customer Id'
                        )
                        ->addColumn(
                            'meeting_id',
                            Table::TYPE_TEXT,
                            255,
                            [],
                            'Meeting id'
                        )
                        ->addColumn(
                            'name',
                            Table::TYPE_TEXT,
                            255,
                            [],
                            'Webinars Name'
                        )
                        ->addColumn(
                            'start_time',
                            Table::TYPE_TIMESTAMP,
                            null,
                            [],
                            'Webinars Start Time'
                        )
                        ->addColumn(
                            'end_time',
                            Table::TYPE_TIMESTAMP,
                            null,
                            [],
                            'Webinars End Time'
                        )
                        ->addColumn(
                            'description',
                            Table::TYPE_TEXT,
                            '64k',
                            [],
                            'Webinars Description'
                        )
                        ->addColumn(
                            'created_at',
                            Table::TYPE_TIMESTAMP,
                            null,
                            ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                            'Created At'
                        )
                        ->addForeignKey(
                            $installer->getFkName(
                                $acsCustomerPersonalTable,
                                'customer_id',
                                $installer->getTable('customer_entity'),
                                'entity_id'
                            ),
                            'customer_id',
                            $installer->getTable('customer_entity'),
                            'entity_id',
                            Table::ACTION_CASCADE
                        )
                        ->setComment('Webinars table');
                $installer->getConnection()->createTable($table);
            }
        }

        if (version_compare($context->getVersion(), '1.0.10', '<')) {
            if (!$installer->tableExists('acs_customer_personal_information')) {
                $acsCustomerPersonalTable = $installer->getTable('acs_customer_personal_information');

                $table = $installer->getConnection()->newTable($acsCustomerPersonalTable)
                        ->addColumn(
                            'id',
                            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                            null,
                            [
                                    'identity' => true,
                                    'nullable' => false,
                                    'primary' => true,
                                    'unsigned' => true,
                                ],
                            'ID'
                        )
                        ->addColumn(
                            'customer_id',
                            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                            null,
                            ['nullable => false'],
                            'customer Id'
                        )
                        ->addColumn(
                            'question',
                            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            '64k',
                            [],
                            'Question'
                        )
                        ->addColumn(
                            'answer',
                            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            '64k',
                            [],
                            'Answer'
                        )
                        ->addColumn(
                            'meeting_id',
                            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            255,
                            ['nullable => false'],
                            'Meeting Id'
                        )
                        ->addColumn(
                            'created_at',
                            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                            null,
                            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                            'Created At'
                        )
                        ->setComment('Customer confidential personal information');
                $installer->getConnection()->createTable($table);
            }
        }

        $installer->endSetup();
    }
}
