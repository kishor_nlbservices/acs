<?php
namespace Webinar\Registration\Controller\Adminhtml\Webinar;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Message\ManagerInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Webinar\Registration\Model\ResourceModel\Webinar;

class Export extends Action {

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var Webinar\CollectionFactory
     */
    protected $webinarCollection;

    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * @var \Webinar\Registration\Model\ResourceModel\PersonalInformation\CollectionFactory
     */
    private $_personalInformationCollectionFactory;

    /**
     * Export constructor.
     * @param Context $context
     * @param Webinar\CollectionFactory $webinarCollection
     */
    public function __construct(
        Context $context,
        Webinar\CollectionFactory $webinarCollection,
        \Webinar\Registration\Model\ResourceModel\PersonalInformation\CollectionFactory $personalInformationCollectionFactory
    ) {
        parent::__construct($context);
        $this->webinarCollection = $webinarCollection;
        $this->messageManager = $context->getMessageManager();
        $this->_personalInformationCollectionFactory = $personalInformationCollectionFactory;
    }

    /**
     * Price Matrix export
     */
    public function execute() {

        try {
            $priceMatrixValues = $this->webinarCollection->create()
                ->addCustomerData();

            if ($priceMatrixValues->getData()) {
                $this->prepareExportedData($priceMatrixValues);
            } else {
                $this->messageManager->addErrorMessage(__('Price matrix empty. Nothing to export.'));
                $this->_redirect('*/*');
            }
        } catch (\Exception $ex) {
            $this->messageManager->addErrorMessage($ex->getMessage());
            $this->_redirect('*/*');
        }
    }

    /**
     * Prepare csv rows for price matrix export
     *
     * @param $priceMatrixValues
     */
    private function prepareExportedData($priceMatrixValues) {
        /** @var $csvHeader csv header */
        $csvHeader = ['meeting_id','name', 'start_time', 'end_time', 'description','source_code','firstname','lastname','email','webinar_api_exception','webinar_api_exception_message', 'created_at','Company/Affiliate Type','Work Function','Post - Secondary Education Completed','Age','How familiar are you with the Webinar Topic?','Would you like to find out more about and hear from the co-producer of this broadcast?','Questions & Comments'];

        $output = fopen('php://output', 'w');
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=webinar_export.csv');
        fputcsv($output, $csvHeader);


        

        foreach ($priceMatrixValues->getData() as $priceMatrix) {

            
            $personalInformationCollectionFactory = $this->_personalInformationCollectionFactory->create();
            $personalInformationCollectionFactory
            ->addFieldToFilter('acs_webinars_id', ['eq' =>$priceMatrix['id']]);
            //->addFieldToFilter('acs_webinars_id', ['eq' =>12]);
            $resultData = $personalInformationCollectionFactory->getData();
            $question=[];
            foreach ($resultData as $resultDataValue) {
                $question[$resultDataValue['question']]=$resultDataValue['answer'];
            }

            $rowOutput = array(
                $priceMatrix['meeting_id'],
                $priceMatrix['name'],
                $priceMatrix['start_time'],
                $priceMatrix['end_time'],
                $priceMatrix['description'],
                $priceMatrix['source_code'],
                $priceMatrix['firstname'],
                $priceMatrix['lastname'],
                $priceMatrix['email'],
                $priceMatrix['exception'] ? 'Failed' : 'Success',
                $priceMatrix['exception_message'],
                $priceMatrix['created_at'],
                (isset($question['Company/Affiliate Type']))?$question['Company/Affiliate Type']:'',
                (isset($question['Work Function']))?$question['Work Function']:'',
                (isset($question['Post - Secondary Education Completed']))?$question['Post - Secondary Education Completed']:'',
                (isset($question['Age']))?$question['Age']:'',
                (isset($question['How familiar are you with the Webinar Topic?']))?$question['How familiar are you with the Webinar Topic?']:'',
                (isset($question['Would you like to find out more about and hear from the co-producer of this broadcast?']))?$question['Would you like to find out more about and hear from the co-producer of this broadcast?']:'',
                (isset($question['Questions & Comments']))?$question['Questions & Comments']:'',
            );

            fputcsv($output, $rowOutput);
        }

        fclose($output);
    }
}