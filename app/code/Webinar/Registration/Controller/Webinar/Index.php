<?php
/**
 * Created by PhpStorm.
 * User: sohelrana
 * Date: 9/16/17
 * Time: 4:47 PM
 */

namespace Webinar\Registration\Controller\Webinar;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Customer;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $customerSession;

    protected $customers;

    public function __construct(
        Context $context,
        Session $customerSession,
        Customer $customers
    )
    {
        parent::__construct($context);
        $this->customerSession = $customerSession;
        $this->customers = $customers;       
    }

    public function execute()
    {

      // echo "helloworld";exit;	
        if ($this->customerSession->isLoggedIn()) {

            //Get customer by customerID.
            $customerId = $this->customerSession->getCustomerData()->getId();
            $customer = $this->customers->load($customerId);
            
                $this->_view->loadLayout(); 
				//$this->_view->getLayout()->getBlock('page.main.title')->setPageTitle('Test TEst');
                $this->_view->renderLayout();  
                //$resultPage->getConfig()->getTitle()->set(__('Privacy Policy'));				
            
        }
        else{
            $url= $this->_url->getUrl('registration/webinar/index');
            $orderCollection = $this->_objectManager->create('Magento\Framework\Url\EncoderInterface')->encode($url);
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('sso/saml2/login',array('referer' => $orderCollection));
            return $resultRedirect;        
        } 
    }
}   