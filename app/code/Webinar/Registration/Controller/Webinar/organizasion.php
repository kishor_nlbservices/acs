<?php
namespace Webinar\Registration\Controller\Webinar;

class Organizasion extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Webinar\Registration\Helper\Config
     */
    private $_helperConfig;

    /**
     * @var \Magento\Directory\Model\Data
     */
    private $_helperData;

    /**
     * @var \Magento\Directory\Model\RealTime
     */
    private $_helperRealTime;
    
    /**
     * @var \Magento\Directory\Model\Region
     */
    private $_region;

    /**
     * @var \Webinar\Registration\Model\ResourceModel\PersonalInformation\CollectionFactory
     */
    private $_personalInformationCollectionFactory;
    
    /**
     * @var \Webinar\Registration\Model\PersonalInformationFactory
     */
    private $_personalInformationFactory;
    
    /**
     * Undocumented variable
     *
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Webinar\Registration\Helper\Config $helperConfig
     * @param \Magento\Directory\Model\Region $region
     * @param \Webinar\Registration\Helper\Data $helperData
     * @param \Webinar\Registration\Helper\RealTime $helperRealTime
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Webinar\Registration\Model\ResourceModel\PersonalInformation\CollectionFactory $personalInformationCollectionFactory
     * @param \Webinar\Registration\Model\PersonalInformationFactory $personalInformationFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Webinar\Registration\Helper\Config $helperConfig,
        \Magento\Directory\Model\Region $region,
        \Webinar\Registration\Helper\Data $helperData,
        \Webinar\Registration\Helper\RealTime $helperRealTime,
        \Magento\Customer\Model\Session $customerSession,
        \Webinar\Registration\Model\ResourceModel\PersonalInformation\CollectionFactory $personalInformationCollectionFactory,
        \Webinar\Registration\Model\PersonalInformationFactory $personalInformationFactory,
		\Magento\Framework\Controller\Result\JsonFactory    $resultJsonFactory
    ) {
        parent::__construct($context);
        $this->_helperConfig=$helperConfig;
        $this->_helperData=$helperData;
        $this->_helperRealTime=$helperRealTime;
        $this->_region=$region;
        $this->customerSession = $customerSession;
        $this->_personalInformationCollectionFactory = $personalInformationCollectionFactory;
        $this->_personalInformationFactory = $personalInformationFactory;
		$this->resultJsonFactory            = $resultJsonFactory;
        //parent::__construct($context);
    }

    /**
     * Execute
     *
     * {@inheritDoc}
     * @see \Magento\Framework\App\ActionInterface::execute()
     */
    public function execute()
    {
	   $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/kishor-auto.log');
       $logger = new \Zend\Log\Logger();
       $logger->addWriter($writer);
	   
	 $result= $this->resultJsonFactory->create();
	 $autocompletetext=$this->getRequest()->getParam('orgValue');
	 $url="https://orgsearch.acs.org/acsoipui/widget/api/getTopOrgs?input=".$autocompletetext;
       $logger->info('==========');
		$autocompleteData = $this->_helperRealTime->curlGet('', $url);
	   $jsonToArrayValue=json_decode($autocompleteData, TRUE);
       $logger->info($jsonToArrayValue);
	   
       $output=[];
       if(is_array($jsonToArrayValue)){
        foreach($jsonToArrayValue as $val){
            $output[]=$val['orgNm'];
        }
       }
       $logger->info($output);
       $logger->info('===s=======');
	 
	   
	   
	 //if ($this->getRequest()->isAjax()) 
        ///{
            $test=Array
            (
                'Firstname' => 'What is your firstname',
                'Email' => 'What is your emailId',
                'Lastname' => 'What is your lastname',
                'Country' => 'Your Country'
            );
			
			
            return $result->setData($output);
            return $result->setData($jsontoarrayvalue);
        //}
	
		
    }
}
