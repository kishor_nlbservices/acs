<?php

namespace Webinar\Registration\Controller\Webinar;

class Save extends \Magento\Framework\App\Action\Action
{

    /**
     * @var \Webinar\Registration\Helper\Config
     */
    private $_helperConfig;

    /**
     * @var \Magento\Directory\Model\Data
     */
    private $_helperData;

    /**
     * @var \Magento\Directory\Model\RealTime
     */
    private $_helperRealTime;

    /**
     * @var \Magento\Directory\Model\Region
     */
    private $_region;

    /**
     * @var \Webinar\Registration\Model\ResourceModel\PersonalInformation\CollectionFactory
     */
    private $_personalInformationCollectionFactory;

    /**
     * @var \Webinar\Registration\Model\PersonalInformationFactory
     */
    private $_personalInformationFactory;
    /**
     * @var  \Webinar\Registration\Model\ErrorReportFactory
     */
    private $_errorReportFactory;

    /**
     * Undocumented variable
     *
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * Undocumented function
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Webinar\Registration\Helper\Config $helperConfig
     * @param \Magento\Directory\Model\Region $region
     * @param \Webinar\Registration\Helper\Data $helperData
     * @param \Webinar\Registration\Helper\RealTime $helperRealTime
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Webinar\Registration\Model\ResourceModel\Webinar\CollectionFactory $webinarCollectionFactory
     * @param \Webinar\Registration\Model\WebinarFactory $webinarFactory
     * @param \Webinar\Registration\Model\ResourceModel\PersonalInformation\CollectionFactory $personalInformationCollectionFactory
     * @param \Webinar\Registration\Model\PersonalInformationFactory $personalInformationFactory
     * @param \Webinar\Registration\Model\ErrorReportFactory $errorReportFactory
     * @param \Magento\Framework\Serialize\Serializer\Json $jsonHelper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Webinar\Registration\Helper\Config $helperConfig,
        \Magento\Directory\Model\Region $region,
        \Webinar\Registration\Helper\Data $helperData,
        \Webinar\Registration\Helper\RealTime $helperRealTime,
        \Magento\Customer\Model\Session $customerSession,
        \Webinar\Registration\Model\ResourceModel\Webinar\CollectionFactory $webinarCollectionFactory,
        \Webinar\Registration\Model\WebinarFactory $webinarFactory,
        \Webinar\Registration\Model\ResourceModel\PersonalInformation\CollectionFactory $personalInformationCollectionFactory,
        \Webinar\Registration\Model\PersonalInformationFactory $personalInformationFactory,
        \Webinar\Registration\Model\ErrorReportFactory $errorReportFactory,
        \Magento\Framework\Serialize\Serializer\Json $jsonHelper
    ) {
        parent::__construct($context);
        $this->_helperConfig = $helperConfig;
        $this->_helperData = $helperData;
        $this->_helperRealTime = $helperRealTime;
        $this->_region = $region;
        $this->_customerSession = $customerSession;
        $this->_webinarCollectionFactory = $webinarCollectionFactory;
        $this->_webinarFactory = $webinarFactory;
        $this->_personalInformationCollectionFactory = $personalInformationCollectionFactory;
        $this->_personalInformationFactory = $personalInformationFactory;
        $this->_errorReportFactory = $errorReportFactory;
        $this->_jsonHelper = $jsonHelper;
    }


    public function validateFields($fields=[]){
        echo "<pre>";

        print_r($this->getRequest()->getParams()); 

        if(!empty($fields)){

        }
        die();
    }
    /**
     * Execute
     *
     * {@inheritDoc}
     * @see \Magento\Framework\App\ActionInterface::execute()
     */
    public function execute()
    {
        /* echo "<pre>";

        print_r($this->getRequest()->getParams()); */
        $fields=['name'];
        $this->validateFields();

        $updateCustomer = false;
        
        $regionId = $this->getRequest()->getParam('region_id');
        $fname = $this->getRequest()->getParam('fname');
        $lname = $this->getRequest()->getParam('lname');
        $email = $this->getRequest()->getParam('email');
        $zipCode = $this->getRequest()->getParam('zipcode');
        $countryId = $this->getRequest()->getParam('country_id');
        $phone = $this->getRequest()->getParam('phone');
        $organization = $this->getRequest()->getParam('organization');
        $meetingId = $this->getRequest()->getParam('meeting_id');
        $sourceCode = $this->getRequest()->getParam('source_code');
        $questions = $this->getRequest()->getParam('question');
        $company = $this->getRequest()->getParam('company');
        $work = $this->getRequest()->getParam('work');
        $secondaryEducation = $this->getRequest()->getParam('secondary_education');
        $age = $this->getRequest()->getParam('age');
        $howFamiliar = $this->getRequest()->getParam('how_familiar');
        $wouldYouLike = $this->getRequest()->getParam('would_you_like');
        $comments = $this->getRequest()->getParam('comments');



        
        $customerId = $this->_customerSession->getCustomer()->getId();
                
        $webinarRegions = $this->_helperData->getWebinarRegions(null, $regionId);
        if (!empty($webinarRegions)) {
            $state = $webinarRegions[0]['code'];
        }

        $webinarCountry = $this->_helperData->getWebinarCountry($countryId);
        if (!empty($webinarCountry)) {
            $country = $webinarCountry[0]['country_id'];
        }
        
        $requestData = [
                        'firstName' => $fname,
                        'lastName' => $lname,
                        'email' => $email,
                        'state' => $state,
                        'zipCode' => $zipCode,
                        'country' => $country,
                        'phone' => $phone,
                        'organization' => $organization
                        ];
        
        $requestData['responses'][] = [
                                        'questionKey' => 0,
                                        'responseText' => null,
                                        'answerKey' => 0
                                        ];
        
        $requestDataSerialize = $this->_jsonHelper->serialize($requestData);
        
        //webinar api call
        $customerData = $this->_helperRealTime->updateCustomerData($requestDataSerialize, $meetingId);



        $webinarError = true;
        $webinarErrorMessage = null;
        if (isset($customerData['success'])) {
            $webinarError = false;
        } else {
            $errorMessages = (isset($customerData['error'])) ? $customerData['error'] : '';
            if (is_array($errorMessages)) {
                foreach ($errorMessages as $error) {
                    $webinarErrorMessage = $error;
                }
            } else {
                $webinarErrorMessage = $errorMessages;
            }
        }
        


        //update webinar data
        $webinarDetails = $this->_helperRealTime->getWebinars($meetingId);
        if (!empty($webinarDetails) && $webinarErrorMessage != 'The user is already registered.') {
            $webinarCollectionFactory = $this->_webinarCollectionFactory->create();
            $webinarFactory = $this->_webinarFactory->create();
            
            /* $webinarCollectionFactory
                    ->addFieldToFilter('meeting_id', ['eq' => $meetingId])
                    ->addFieldToFilter('customer_id', ['eq' => $customerId]);
            $firstCollectionFactory = $webinarCollectionFactory->getFirstItem();
            $resultData = $firstCollectionFactory->getData(); */
            $webinarData = [
                            'meeting_id' => $meetingId,
                            'source_code' => $sourceCode,
                            'customer_id' => $customerId,
                            'name' => $webinarDetails['name'],
                            'start_time' => date('Y-m-d H:i:s', strtotime($webinarDetails['startTime'])),
                            'end_time' => date('Y-m-d H:i:s', strtotime($webinarDetails['endTime'])),
                            'description' => $webinarDetails['description'],
                            'exception' => $webinarError,
                            'exception_message' => $webinarErrorMessage,
                            'firstname' => $fname,
                            'lastname' => $lname,
                            'company' => $company,
                            'work' => $work,
                            'secondary_education' => $secondaryEducation,
                            'age' => $age,
                            'how_familiar' => $howFamiliar,
                            'would_you_like' => $wouldYouLike,
                            'comments' => $comments,
                            ];

            /* if (!empty($resultData)) {
                $webinarData['id'] = $resultData['id'];
            } */
            
            $webinarDetails = $webinarFactory->setData($webinarData)->save();
            $webinarId = $webinarDetails->getId();

            //update question
            /* foreach ($questions as $key => $value) {
                if ($value) {
                    $data = [
                            'meeting_id' => $meetingId,
                            'customer_id' => $customerId,
                            'question' => $key,
                            'answer' => $value,
                            'acs_webinars_id' => $webinarId,
                            ];

                    $personalInformationCollectionFactory = $this->_personalInformationCollectionFactory->create();
                    $personalInformationFactory = $this->_personalInformationFactory->create();

                    $personalInformationCollectionFactory
                            ->addFieldToFilter('meeting_id', ['eq' => $meetingId])
                            ->addFieldToFilter('question', ['eq' => $key]);
                    $firstCollectionFactory = $personalInformationCollectionFactory->getFirstItem();
                    $resultData = $firstCollectionFactory->getData();
                    if (!empty($resultData)) {
                        $data['id'] = $resultData['id'];
                    }

                    $personalInformationFactory->setData($data)->save();
                }
            } */

            
            //error report
            /* print_r($customerData);
            print_r($webinarData); */

            if ($webinarError) {
                $data = [
                'error_type' => 'Webinar Error',
                'error_massage' => $webinarErrorMessage,
                'request_data' => $requestDataSerialize,
                'response' => $this->_jsonHelper->serialize($customerData),
                'customer_id' => $customerId,
                'webinar_id' => $webinarId,
                'meeting_id' => $meetingId,
                ];
                //print_r($data);
                $this->_errorReportFactory->create()->setData($data)->save();
            }
            //error report
        }
          
        //net-forum
        $netForumError = '';
        $netForumErrorMessage = null;
        $membertype = '';
        
        $startDate = null;
        $endDate = null;
        
        if ($this->getRequest()->getParam('free_registration')) {
            $customerData = $this->_helperData->getCustomerSessionData();
            $constituentId = (isset($customerData['constituentid'])) ? $customerData['constituentid'] : '';
            $membertype = 'COMMUNITY_MEMBER';
            
            $startDate = date("Y-m-d");
            $endDate = strtotime(date("Y-m-d", strtotime($startDate)) . " +6 month");
            $endDate = date("Y-m-d", $endDate);
            
            $requestData = [
                            'constituentid' => $constituentId,
                            'applicationtype' => 'JOIN',
                            'membertype' => 'COMMUNITY_MEMBER',
                            'cencchoice' => 'DIGITAL',
                            'period' => 'MONTH',
                            'length' => 6,
                            'purchasedatetime' => date('Y-m-d') . 'T' . date('h:i:s'),
                            'price' => 0,
                            'externalpackagecode' => 'P-A0',
                            'attestationdate' => date('Y-m-d') . 'T' . date('h:i:s'),
                            'attestationname' => $fname . ' ' . $lname,
                            'firstname' => $fname,
                            'lastname' => $lname,
                            'emailaddress' => $email,
                            'sourcecode' =>$sourceCode
                            ];
            
            $requestDataSerialize = $this->_jsonHelper->serialize($requestData);
            
            $netForum = $this->_helperRealTime->netForumUpdate($requestDataSerialize);
            
            if (isset($netForum['success'])) {
                $netForumError = 'Success';
            } else {
                $membertype = '';
                $startDate = null;
                $endDate = null;
                $netForumError = 'Failed';
                $errorMessages = (isset($netForum['error'])) ? $netForum['error'] : '';
                if (is_array($errorMessages)) {
                    foreach ($errorMessages as $error) {
                        $netForumErrorMessage = $error;
                    }
                } else {
                    $netForumErrorMessage = $errorMessages;
                }
            }
            


            
            //error report
            //print_r($customerData);

            if ($netForumErrorMessage) {
                $data = [
                'error_type' => 'Netforum Error',
                'error_massage' => $netForumErrorMessage,
                'request_data' => $requestDataSerialize,
                'response' => $this->_jsonHelper->serialize($netForum),
                'customer_id' => $customerId,
                'meeting_id' => $meetingId,
                ];
                //print_r($data);
                $this->_errorReportFactory->create()->setData($data)->save();
            }
            //error report

            $this->_customerSession->setNetForumRegister(true);

            $updateCustomer = true;
        }
        
        //die();
        //update customer
        $data = [
                'firstname' => $fname,
                'lastname' => $lname,
                'country_id' => $countryId,
                'postcode' => '.',
                'region_id' => $regionId,
                'company' => $organization,
                'membertype' => $membertype,
                'startDate' => $startDate,
                'endDate' => $endDate,
                'member_api_exception' => $netForumError,
                'member_api_exception_message' => $netForumErrorMessage,
                ];
        
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/customer.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info('----');
        $logger->info($data);

        $this->_helperData->createCustomerAddress($data, $updateCustomer);
        if ($updateCustomer) {
            $regionCode = $this->_helperData->createOrderPackageA();
        }
        
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('registration/webinar/success');
        
        return $resultRedirect;
    }
}
