<?php

namespace Webinar\Registration\Controller\Webinar;

use Magento\Framework\App\Action\Context;

class MassCountryInsert extends \Magento\Framework\App\Action\Action {

    public function __construct(
            Context $context,
            \Magento\Framework\Module\Dir\Reader $moduleReader,
            \Magento\Framework\File\Csv $csv,
            \Magento\Directory\Model\ResourceModel\Region\CollectionFactory $regionCollectionFactory,
            \Magento\Directory\Model\ResourceModel\Country\CollectionFactory $countryCollectionFactory,
            \Webinar\Registration\Model\ResourceModel\Country\CollectionFactory $webinarCountryCollectionFactory,
            \Webinar\Registration\Model\ResourceModel\Region\CollectionFactory $webinarRegionCollectionFactory,
            \Webinar\Registration\Model\CountryFactory $webinarCountryFactory,
            \Webinar\Registration\Model\RegionFactory $webinarRegionFactory
            ) {
        
        $this->moduleReader = $moduleReader;
        $this->csv = $csv;
        $this->regionCollectionFactory = $regionCollectionFactory;
        $this->countryCollectionFactory = $countryCollectionFactory;
        $this->webinarCountryCollectionFactory = $webinarCountryCollectionFactory;
        $this->webinarRegionCollectionFactory = $webinarRegionCollectionFactory;
        $this->webinarCountryFactory = $webinarCountryFactory;
        $this->webinarRegionFactory = $webinarRegionFactory;
        
        parent::__construct($context);
    }
    
    public function getModuleDataDirectory() {
        $viewDir = $this->moduleReader->getModuleDir(\Magento\Framework\Module\Dir::MODULE_VIEW_DIR, 'Webinar_Registration');
        $dataDir = dirname($viewDir) . '/data';
        
        return $dataDir;
    }
    
    public function readCSVFile($file) {
        $fullpath = $this->getModuleDataDirectory() . '/' . $file;
        
        $csvData = $this->csv->getData($fullpath);
        
        return $csvData;
    }
    
    public function getMagentoCountryList() {
        $collection = $this->countryCollectionFactory->create()->loadByStore()->toOptionArray();
        
        $countries = [];
        foreach ($collection as $country) {
            $countries[$country['label']] = $country['value'];
        }
        
        return $countries;
    }
    
    public function getMagentoRegionList() {
        $collection = $this->countryCollectionFactory->create()->loadByStore()->toOptionArray();
        $countryIds = [];
        foreach ($collection as $country) {
            $countryIds[] = $country['value'];
        }
        
        $collection = $this->regionCollectionFactory->create()->addCountryFilter($countryIds)->load();
        $regions = [];
        foreach ($collection as $region) {
            /** @var $region \Magento\Directory\Model\Region */
            if (!$region->getRegionId()) {
                continue;
            }
            
            $regions[$region->getCountryId()][$region->getRegionId()] = [
                'code' => $region->getCode(),
                'name' => (string)__($region->getName()),
            ];
        }
        
        return $regions;
    }
    
    public function execute() {
        $csvData = $this->readCSVFile('country.csv');
        $magentoCountryList = $this->getMagentoCountryList();
        
        // save the data in `acs_directory_country` table
        foreach ($csvData as $data) {
            $name = $data[0];
            
            if (!empty($magentoCountryList[$name])) {
                $countryId = $name;
                $countryName = $name;
                $mageCountryId = $magentoCountryList[$name];
                
                // check if the data already exist / not
                $webinarCountryCollection = $this->webinarCountryCollectionFactory->create();
                $checkData = $webinarCountryCollection->addFieldToFilter('country_id', ['eq' => $countryId])->load();
                
                if ($checkData->getSize() < 1) {
                    $webinarCountry = $this->webinarCountryFactory->create();
                    $webinarCountry
                            ->setData('country_id', $countryId)
                            ->setData('country_name', $countryName)
                            ->setData('mage_country_id', $mageCountryId)
                            ->save()
                            ->unsetData();
                }
            }
        }
        
        $csvData = $this->readCSVFile('country-state.csv');
        $magentoRegionList = $this->getMagentoregionList();
        
        // save the data in `acs_directory_country_region` table
        foreach ($csvData as $data) {
            $countryName = $data[0];
            $regionName = $data[1];
            
            if (!empty($magentoRegionList[$countryName])) {
                $webinarCountryCollection = $this->webinarCountryCollectionFactory->create();
                $countryData = $webinarCountryCollection->addFieldToFilter('mage_country_id', ['eq' => $countryName])->load();
                $webinarCountryId = 0;
                $mageCountryId = '';
                foreach ($countryData as $data) {
                    $webinarCountryId = $data->getId();
                }
                
                foreach ($magentoRegionList[$countryName] as $regionKey => $regionVal) {
                    if ($regionVal['name'] == $regionName) {
                        $code = $regionName;
                        $name  = $regionName;
                        $countryId = $webinarCountryId;
                        $mageCountryId = $countryName;
                        $mageRegionId = $regionKey;
                        
                        // check if the data already exist / not
                        $webinarRegionCollection = $this->webinarRegionCollectionFactory->create();
                        $checkData = $webinarRegionCollection
                                                    ->addFieldToFilter('code', ['eq' => $code])
                                                    ->addFieldToFilter('name', ['eq' => $name])
                                                    ->addFieldToFilter('country_id', ['eq' => $countryId])
                                                    ->addFieldToFilter('mage_country_id', ['eq' => $mageCountryId])
                                                    ->addFieldToFilter('mage_region_id', ['eq' => $mageRegionId])
                                                    ->load();
                        
                        if ($checkData->getSize() < 1) {
                            $webinarRegion = $this->webinarRegionFactory->create();
                            $webinarRegion
                                    ->setData('code', $code)
                                    ->setData('name', $name)
                                    ->setData('country_id', $countryId)
                                    ->setData('mage_country_id', $mageCountryId)
                                    ->setData('mage_region_id', $mageRegionId)
                                    ->save()
                                    ->unsetData();
                        }
                    }
                }
            }
        }
        
        echo "Hi";
        die();
    }

}
