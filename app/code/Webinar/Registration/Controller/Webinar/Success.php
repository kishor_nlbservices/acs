<?php
/**
 * Created by PhpStorm.
 * User: sohelrana
 * Date: 9/16/17
 * Time: 4:47 PM
 */

namespace Webinar\Registration\Controller\Webinar;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Customer;

class Success extends \Magento\Framework\App\Action\Action
{
    protected $customerSession;

    protected $customers;

    public function __construct(
        Context $context,
        Session $customerSession,
        Customer $customers
    )
    {
        parent::__construct($context);
        $this->customerSession = $customerSession;
        $this->customers = $customers;       
    }

    public function execute()
    {
        $lastCustomerId = $this->customerSession->getId();
        $this->customerSession->logout()->setBeforeAuthUrl($this->_redirect->getRefererUrl())
            ->setLastCustomerId($lastCustomerId);
        $this->_view->loadLayout(); 
        $this->_view->renderLayout();
    }
}   