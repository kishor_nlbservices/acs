<style>
div[style] {
    padding:0px !important;
  
}
</style>
<!--====== start  page content ====== -->

    <div id="bd" role="main">
        <!--=== main well start ===-->

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                
                    <div class="articleContent parsys">
                        <div class="herocontainer parbase section">
                            <div class="container-fluid webinar-list">
                                <div class="clearfix text_dark">
                                    <div class="container">
                                        <div class="containerPar parsys">
                                            <div class="herocontainer parbase section">
                                                <div class="container">
                                                    <div class="clearfix text_dark">
                                                        <div class="container p-35-0">
                                                            <div class="col-md-12 p-0">
                                                                <div class="row rowEven balanced">
                                                                    <div class="col-md-4 col-xs-12 columnOne columnOdd" style="height: auto;">
                                                                        <div class="column0 parsys">
                                                                            <div class="image parbase section">
                                                                                <div class="container">

                                                                                    <div class="image-div-logo">
<img src="{{media url=image/acs-webinar-logo.png}}" alt="webinar logo" />
                                                                                        
                                                                                    </div>

                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-5 col-xs-12 columnTwo columnEven" style="height: auto;">
                                                                        <div class="column1 parsys">
                                                                            <div class="container">
                                                                                <div class="text parbase section">
                                                                                    <div class="container ">
                                                                                        <div class="">
                                                                                            <h3>Learn from the best & brightest minds in chemistry</h3> The two biggest existential environmental threats to human survival
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3 col-xs-12 columnThree columnOdd" style="height: auto;">
                                                                        <div class="text text-right parbase section">
                                                                            <div class="container ">
                                                                                <div class="webinar-banner-text">
                                                                                    <h4>2 PM ET</h4>
                                                                                    <div><b class="webinar-live">LIVE</b><b>Every Thursday</b></div>
                                                                                    <a href="#" class="btn mt-10" style="background:#fff; color:#0054a5;border: 1px solid #0054a5;">Join Our Mailing List!</a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="herocontainer parbase section">
                                <div class="container">
                                    <div class="clearfix text_dark">
                                        <div class="container p-35-0">
                                            <div class="col-md-12 p-0">
                                                <div class="text parbase section">
                                                    <div class="container">
                                                        <div class="">
                                                            <h3 style="color#7f7f7f;">We have a topic of interest for every chemist.<br> Go ahead, dive into these diverse categories and discover our growing archive of past webinars.</h3>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="text parbase section">
                                                    <div class="container ">
                                                        <div class="past-webinar">
                                                            <ul class="list-inline">
                                                                <li class="list-inline-item"><a href="#">Business & Entreprenuership</a></li>
                                                                <li class="list-inline-item"><a href="#">Professional Development</a></li>
                                                                <li class="list-inline-item"><a href="#">Technology & Innovation</a></li>
                                                                <li class="list-inline-item"><a href="#">Drug Discovery</a></li>
                                                                <li class="list-inline-item"><a href="#">Culinary Chemistry</a></li>
                                                                <li class="list-inline-item"><a href="#">Popular Chemistry</a></li>
                                                                
                                                            </ul>

                                                        </div>
                                                    </div>
                                                </div>
                                                

                                            </div>

                                        </div>
                                        
                                    </div>
                                    <hr class="hr-no-gap">
                                </div>

                            </div>
                                <div class="herocontainer parbase section">
                                <div class="container container-fluid  bottom-promo">
                                    <div class="clearfix text_dark" id="membership-content-mem">
                                        <div class="container">
                                            <div class="containerPar parsys">
                                                <div class="columnsBootstrap parbase section">
                                                    <div class="container">
                                                        <div class="row rowOdd membership-section-5">
                                                            <div class="text cq-dd-image clear">
                                                                <h1 class="mb-30">Upcoming Free Webinar</h1>

                                                            </div>
                                                            <div class="col-md-4 col-xs-12 columnOne columnOdd pl-0">
                                                                <div class="column0 parsys">
                                                                    <div class="textimage parbase section">
                                                                        <div class="container ">
                                                                            <div class="clearfix upcoming-free-webinar">
                                                                                <div class="image-div">
                                                                                <a href="{{config path="web/unsecure/base_url"}}webinar-details">
                                                                                    <div class="image">
<img src="{{media url=image/1561046271387.png}}" alt="webinar" class="cq-dd-image"/>
                                                                                       
                                                                                    </div>
                                                                                </a>
                                                                            </div>
                                                                                   
                                                                            
                                                                                <div class="text cq-dd-image clear">
                                                                                    <a href="{{config path="web/unsecure/base_url"}}webinar-details">
                                                                                        <h3><b>Combating Climate Change with New
Nanobugs: Teaching Bacteria to Eat
Carbon Dioxide and Light with
Quantum Dots</b></h3></a>
                                                                                    <div class="upcoming-webinar-date">Sunday, March 01, 2020</div>
                                                                                    <p>Join Prashant Nagpal of the University of Colorado Boulder and the Antimicrobial Regeneration Consortium ashe discusses the development of living nano-biohybrid organisms that usesunlight to convert massive amounts of carbon dioxide, air, and </p>
                                                                                    <a href="javascript:void(0);" id="reg-webinar1" class="btn btn-primary mt-10 reg-webinar">Register For Free</a>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 col-xs-12">
                                                                <div class="column1 parsys">
                                                                    <div class="textimage parbase section">
                                                                        <div class="container ">
                                                                            <div class="clearfix upcoming-free-webinar">
                                                                                <div class="image-div">
                                                                                    <a href="{{config path="web/unsecure/base_url"}}webinar-details">
                                                                                        <div class="image">
<img src="{{media url=image/1567111774008.png}}" alt="webinar" class="cq-dd-image"/>
                                                                                            
                                                                                        </div>
                                                                                    </a>
                                                                                  
                                                                                </div>
                                                                                <div class="text cq-dd-image clear">
                                                                                    <a href="{{config path="web/unsecure/base_url"}}webinar-details"><h3><b>Phase Separation of Multivalent
Proteins: Recent Findings and New
Frontiers</b></h3></a>
                                                                                    <div class="upcoming-webinar-date">Sunday, March 01, 2020</div>
                                                                                    <p>Join Rohit Pappu of Washington University in St. Louis as he discusses the emerging aspects of multivalent proteins usingastickers-and-spacers framework, which will allowyou to better understand the driving forces and functional implications of phase separation, </p>
                                                                                    <a href="javascript:void(0);" id="reg-webinar2" class="btn btn-primary mt-10 reg-webinar">Register For Free</a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                             <div class="col-md-4 col-xs-12">
                                                                <div class="column1 parsys">
                                                                    <div class="textimage parbase section">
                                                                        <div class="container ">
                                                                            <div class="clearfix upcoming-free-webinar">
                                                                                <div class="image-div">
                                                                                    <a href="{{config path="web/unsecure/base_url"}}webinar-details">
                                                                                        <div class="image">
<img src="{{media url=image/1566323909578.png}}" alt="webinar" class="cq-dd-image"/>
                                                                                           
                                                                                        </div>
                                                                                    </a>
                                                                                   
                                                                                </div>
                                                                                <div class="text cq-dd-image clear">
                                                                                    <a href="{{config path="web/unsecure/base_url"}}webinar-details"><h3><b>Phase Separation of Multivalent
Proteins: Recent Findings and New
Frontiers</b></h3></a>
                                                                                    <div class="upcoming-webinar-date">Sunday, March 01, 2020</div>
                                                                                    <p>Join Rohit Pappu of Washington University in St. Louis as he discusses the emerging aspects of multivalent proteins usingastickers-and-spacers framework, which will allowyou to better understand the driving forces and functional implications of phase separation, </p>
                                                                                    <a href="javascript:void(0);" id="reg-webinar3" class="btn btn-primary mt-10">Register For Free</a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /row-fluid -->
        </div>

    </div>

    <!--====== end  page content ====== -->
<div id="custom_popup2" style="display:none;">
    <div class="login">
        <img src="{{media url=image/login.jpg}}" alt="" width="100%" height="550px" />
    </div>
    <fieldset class="fieldset">
        <div class="field">
  
            <div class="control">
                <input type="checkbox" name="checkbox" id="checkbox2" class="checkbox" title=""> Accept Login
            </div>
        </div>
    </fieldset>
</div>
<script>
    require(
        [
            'jquery',
            'Magento_Ui/js/modal/modal'
        ],
        function(
            $,
            modal
        ) {
            var options = {
                type: 'popup',
                responsive: true,
                innerScroll: false,
                buttons: [{
                    text: $.mage.__('OK'),
                    class: '',
                    click: function () {
                        if($("#checkbox2" ).is(':checked')){
                          window.location='{{config path="web/unsecure/base_url"}}webinar-registration-form';
                        } else{
                            alert("Please accept to login");

                        }
                    }
                }]
            };

            var popup = modal(options, $('#custom_popup2'));
            $("#reg-webinar1").on('click',function(){ 
                $("#custom_popup2").modal("openModal");
            });
          $("#reg-webinar2").on('click',function(){ 
                $("#custom_popup2").modal("openModal");
            });
            $("#reg-webinar3").on('click',function(){ 
                $("#custom_popup2").modal("openModal");
            });

        }
    );
</script>