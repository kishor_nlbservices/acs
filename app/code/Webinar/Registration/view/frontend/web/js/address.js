define([
    'jquery',
    'ko',
    'uiComponent',
    'mage/translate',
    'jquery/ui'
], function (
    $,
    ko,
    Component,
    $t
) {
    'use strict';
    return Component.extend({
        defaults: {
            activeMethod: ''
        },
        availableCountryOption: ko.observableArray([]),
        availableRegionOption: ko.observableArray([]),
        organizationList: ko.observableArray([]),
        chosenCountry: ko.observable(),
        chosenRegion: ko.observable(),
        countryList: ko.observableArray([]),
        regionList: ko.observableArray([]),
        organizasion_url: '',
        /** @inheritdoc */
        initialize: function (config) {
            this._super();
            this.timer = 0;
            this.countryList = config.countries;
            this.regionList = config.regions;
            this.chosenCountry = config.chosenCountry;
            this.chosenRegion = config.chosenRegion;
            this.organizasion_url = config.organizasionUrl;
            this.setCountry();
            return this;
        },
        setCountry: function () {

            var pusTis = this,
		    chosenRegion=this.chosenRegion;
            
            /* $.each(this.regionsList, function (key, value) {
                var countryCode = value.stateTerritoryType.countryCode;
                countryCode = countryCode.toLowerCase();
                Country_list[countryCode] = countryCode;
            }); */

            // pusTis.availableCountryOption.push({ 'value': 'US', 'label':'United States'});
            /* $.each(this.country, function (key, value) {
                var label = value.label.toLowerCase();
                if (value.value && Country_list[label] !== undefined) {
                    pusTis.availableCountryOptions.push({ 'value': value.value, 'label': value.label });
                }
            }); */
            // pusTis.chosenCountry = pusTis.chosenCountry;
            
            if (pusTis.chosenCountry) {
                setTimeout(function () {
			console.log(chosenRegion); 
			pusTis.setRegion(pusTis.chosenCountry,chosenRegion); 
		}, 100);
            }

        },
        setRegion: function (country_code,region_id) {
            var countryCode = country_code,
                pusTis = this;

            /* $.each(pusTis.country, function (key, value) {
                if (city == value.value && city != '') {
                    city_name = value.label;
                }
            }); */
            /* $.each(pusTis.regions, function (key, value) {
                var countryCode = value.stateTerritoryType.countryCode;
                countryCode = countryCode.toLowerCase();
                city_name = city_name.toLowerCase();
                if (value.stateTerritoryType.code && country_code == countryCode) {
                    pusTis.availableRegionOptions.push({ 'value': value.stateTerritoryType.code, 'label': value.stateTerritoryType.name });
                }
            }); */


            $.each(pusTis.regionList, function (key, value) {
                if (countryCode == key) {
                    $.each(value, function (regionKey, regionValue) {
                        pusTis.availableRegionOption.push({ 'value': regionKey, 'label': regionValue.name });
                    });
                }
            });
            
            $('#region_id option[value="' + region_id + '"]').attr('selected', 'selected');
        },
        delay :(function(){
            var timer = 0;
            return function(callback, ms){
               clearTimeout (timer);
               timer = setTimeout(callback, ms);
            };
            })(),
        organizationChange: function (place, event) {
            var object = this;
            var search = event.currentTarget.value;
            var delay = 1000;
            $('.autocompletetext ul').css("display","none");
            $.each(object.organizationList(), function (key, value) {
                object.organizationList().pop();
            });
            if (search != '' && search.length > 2) {
                var url = object.organizasion_url,
                    pusTis=this;
                    object.delay(function(){
                        $.ajax({
                            url: url,
                            type: 'POST',
                            dataType: 'json',
                            showLoader: true,
                            data: { orgValue: event.currentTarget.value },
                            complete: function (response) {
                                if (response.responseJSON.length > 0) {
                                    $('.autocompletetext ul').css("display","block");
                                    $.each(response.responseJSON, function (key, value) {
                                        if (value != '') {                                      
                                            pusTis.organizationList.push(value);
                                        }
                                        else {                                        
                                            pusTis.organizationList.push('No Result Found');
                                        }
                                    });
                                }
                            },
                            error: function (xhr, status, errorThrown) {
                                console.log('Error happens. Try again.');
                            }
                        });
         
                        
                     }, 600 );
            }
        },
		ChangeCemail: function (place, event) {
            var object = this;
            var cvalue = event.currentTarget.value;
			$('.emailvalid').hide();
			var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			  if(!regex.test(cvalue)) {
				$('.emailvalid').show();
				$('.convalid').hide();
                $("#webinar_submit").attr("disabled", true);					
			  }else{
				$('.emailvalid').hide();
				$('.convalid').hide();	
				 var emailm=$('#email').val();
				if(emailm!=cvalue)
				{
				$('.convalid').show();
                $('.emailvalid').hide();
                $("#webinar_submit").attr("disabled", true);				
				}else{
				$('.convalid').hide();	
                $('.emailvalid').hide();
                $("#webinar_submit").attr("disabled", false);				
				}
				
			  }
			console.log(cvalue);
        },
        selectOrganizationList:function(place, event){
            var textValue=event.target.innerText,
                pusTis=this;
            $('#organization').val(textValue);
            $('.autocompletetext ul').css("display","none");
        },
        
        changeCountry: function (place, event) {
            var countryCode = event.currentTarget.value,
                pusTis = this,
                regionExist=false;
            
            $.each(pusTis.availableRegionOption(), function() {
                pusTis.availableRegionOption.pop();
            });
            
            $.each(pusTis.regionList, function (key, value) {
                if (countryCode == key) {
                	regionExist=true    
                    $.each(value, function (regionKey, regionValue) {
                        pusTis.availableRegionOption.push({ 'value': regionKey, 'label': regionValue.name });
                    });
                }
            });
            if(!regionExist){
            	$('#region_id').parent().parent().find('span.required').css('display','none'); 
            	$('#region_id').css('display','none');            	
            	$('#region').css('display','block');
            	$("#region_id").removeAttr("name");
            	$("#region").attr("name","region");
            }else{            	
            	$('#region_id').parent().parent().find('span.required').removeAttr("style");
            	$('#region_id').css('display','block');            	
            	$('#region').css('display','none');
            	$("#region").removeAttr("name");
            	$("#region_id").attr("name","region_id");
            }
           /*  $.each(this.country, function (key, value) {
                if (city == value.value && city != '') {
                    city_name = value.label;
                }
            });
            $.each(pusTis.regions, function (key, value) {
                var countryCode = value.stateTerritoryType.countryCode;
                countryCode = countryCode.toLowerCase();
                city_name = city_name.toLowerCase();
                if (value.stateTerritoryType.code && city_name == countryCode) {
                    pusTis.availableRegionOptions.push({ 'value': value.stateTerritoryType.code, 'label': value.stateTerritoryType.name });
                }
            }); */
        }
    });
});
