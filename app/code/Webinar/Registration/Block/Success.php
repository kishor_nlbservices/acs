<?php
namespace Webinar\Registration\Block;

class Success extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Directory\Block\Data
     */
    protected $directoryBlock;    
    
    /**
     * @var \Magento\Directory\Model\Data
     */
    private $_helperData;
    
    /**
     * @var \Magento\Directory\Model\Config\Source\Country
     */
    private $_countryModel;

    protected $customerSession;

    protected $helperRealTime;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Directory\Block\Data $directoryBlock
     * @param \Webinar\Registration\Helper\Data $helperData
     * @param \Magento\Directory\Model\Config\Source\Country $countryModel
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Webinar\Registration\Helper\RealTime $helperRealTime
     * @param \Magento\Framework\App\Request\Http $request
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Directory\Block\Data $directoryBlock,
        \Webinar\Registration\Helper\Data $helperData,
        \Magento\Directory\Model\Config\Source\Country $countryModel,
        \Magento\Customer\Model\Session $customerSession,
        \Webinar\Registration\Helper\RealTime $helperRealTime,
        \Magento\Framework\App\Request\Http $request,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->directoryBlock = $directoryBlock;
        $this->_helperData=$helperData;
        $this->_countryModel=$countryModel;
        $this->customerSession = $customerSession;
        $this->helperRealTime = $helperRealTime;
        $this->request = $request;
    }
 
    public function _prepareLayout()  
    {
        $this->pageConfig->getTitle()->set(__('Webinar Registration Success'));
        return parent::_prepareLayout();  
    }

    public function getMeetingId() {
        return $this->request->getParam('id');
    }

    public function getCountries($defValue = null)
    {
        return $this->_countryModel->toOptionArray();
    }
    public function getRegion($value = null)
    {        
        $getState = $this->customerSession->getStateData();
        if(!$getState){
            $getState = $this->helperRealTime->getState();
            $this->customerSession->setStateData($getState);
        }
        return $getState;
    }
    public function getCountryAction()
    {
        return $this->getUrl('extension/extension/country', ['_secure' => true]);
    }

    public function getAddressData($field)
    {
        $output=null;
        $address=$this->_helperData->getDefaultBillingAddress();
        if(!empty($address)){
            $output=$address[$field];
            if($field=='region_id' && $output){
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $region = $objectManager->create('Magento\Directory\Model\Region')
                                        ->load($output);
                $output=$region->getData('code');
            }
        }
        return $output;
    }
}
