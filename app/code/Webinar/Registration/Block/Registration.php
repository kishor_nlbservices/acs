<?php

namespace Webinar\Registration\Block;

class Registration extends \Magento\Framework\View\Element\Template {

    /**
     * @var \Magento\Directory\Block\Data
     */
    protected $_directoryBlock;

    /**
     * @var \Magento\Directory\Model\Data
     */
    private $_helperData;

    /**
     * @var \Magento\Directory\Model\Config\Source\Country
     */
    private $_countryModel;
    protected $_customerSession;
    protected $_helperRealTime;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Directory\Block\Data $directoryBlock
     * @param \Webinar\Registration\Helper\Data $helperData
     * @param \Magento\Directory\Model\Config\Source\Country $countryModel
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Webinar\Registration\Helper\RealTime $helperRealTime
     * @param \Magento\Framework\App\Request\Http $request
     * @param array $data
     */
    public function __construct(
            \Magento\Framework\View\Element\Template\Context $context,
            \Magento\Directory\Block\Data $directoryBlock,
            \Webinar\Registration\Helper\Data $helperData,
            \Magento\Directory\Model\Config\Source\Country $countryModel,
            \Magento\Customer\Model\Session $customerSession,
            \Webinar\Registration\Helper\RealTime $helperRealTime,
            \Magento\Framework\App\Request\Http $request,
            \Webinar\Registration\Model\ResourceModel\Country\CollectionFactory $webinarCountryCollectionFactory,
            \Magento\Directory\Model\ResourceModel\Country\CollectionFactory $countryCollectionFactory,
            \Magento\Framework\Serialize\Serializer\Json $jsonHelper,
            array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_directoryBlock = $directoryBlock;
        $this->_helperData = $helperData;
        $this->_countryModel = $countryModel;
        $this->_customerSession = $customerSession;
        $this->_helperRealTime = $helperRealTime;
        $this->_request = $request;
        $this->_webinarCountryCollectionFactory = $webinarCountryCollectionFactory;
        $this->_countryCollectionFactory = $countryCollectionFactory;
        $this->_jsonHelper = $jsonHelper;
    }

    public function _prepareLayout() {
        $this->pageConfig->getTitle()->set(__('Webinar Registration Form'));
        
        return parent::_prepareLayout();
    }

    public function getMeetingId() {
        return $this->_request->getParam('id', 0);
    }
    
    public function getSourceCode() {
        return $this->_request->getParam('sc');
    }
    
    protected function getTopDestinations() {
        $destinations = (string)$this->_scopeConfig->getValue(
                'general/country/destinations',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        
        return !empty($destinations) ? explode(',', $destinations) : [];
    }

    public function getCountries($defValue = null) {
        $countries = $this->_countryCollectionFactory->create()->loadByStore()
                                ->setForegroundCountries($this->getTopDestinations())
                                ->toOptionArray();
        
        return $this->_jsonHelper->serialize($countries);
    }
    
    public function getAddressData($field) {
        $output = null;
        $address = $this->_helperData->getDefaultBillingAddress();
        
        if (!empty($address)) {
            $output = $address[$field];
        }
        
        return $output;
    }

    public function getWebminarName() {
        $meetingId = $this->getMeetingId();

        $webminarDetail = $this->_helperRealTime->getWebinars($meetingId);


        return $webminarDetail['name'];
    }

    public function getWebminarStartTime() {
        $meetingId = $this->getMeetingId();

        $webminarDetail = $this->_helperRealTime->getWebinars($meetingId);

        return date('D, M j, Y h:i A', strtotime($webminarDetail['startTime']));
    }

    public function getWebminarEndTime() {
        $meetingId = $this->getMeetingId();

        $webminarDetail = $this->_helperRealTime->getWebinars($meetingId);

        return date('h:i A', strtotime($webminarDetail['endTime']));
    }

    public function getWebminarDescription() {
        $meetingId = $this->getMeetingId();

        $webminarDetail = $this->_helperRealTime->getWebinars($meetingId);

        return $webminarDetail['description'];
    }

}
