<style>
div[style] {
    padding:0px !important;
  
}
</style>
<!--====== start  page content ====== -->

    <div id="bd" role="main">
        <!--=== main well start ===-->

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="articleContent parsys">
                        <div class="herocontainer parbase section">
                            <div class="container-fluid ">
                                <div class="clearfix text_dark" id="acs-cover-section">
                                    <div class="container">
                                        <div class="containerPar parsys">
                                            <div class="herocontainer parbase section">
                                                <div class="container">
                                                    <div class="clearfix text_dark">
                                                        <div class="container p-35-0">
                                                            <div class="col-md-12">
                                                                <div class="text parbase section">
                                                                    <div class="container">
                                                                        <div>
                                                                            <h1 class="align-section-left">Combating Climate Change with New Nanobugs:<br>Teaching Bacteria to Eat Carbon Dioxide and Light <br>with Quantum Dots</h1>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="text parbase section">
                                                                    <div class="container">
                                                                        <div>
                                                                            <p><a href="#">ACS Webinars</a>&nbsp; | &nbsp;November 07,2019</p>
                                                                            <a id="reg1" href="#" class="btn btn-primary">Register for Free</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="herocontainer parbase section">
                                <div class="container">
                                    <div class="clearfix text_dark">
                                        <div class="container">
                                            <div class="col-md-12 p-0">
                                                <div class="col-md-8">
                                                    <div class="text parbase section">
                                                        <div class="container">
                                                            <div>
                                                                <p>The two biggest existential environmental threats to human survival on the planet are climate change due to excessive emission of greenhouse gases and the 10,000-yearlifecycle plastic pollution. How asasociety can we address these issues simultaneously and through an inexpensive, sustainable,and scalable technology?</p>
                                                                <p>Join Prashant Nagpal of the University of Colorado Boulder and the Antimicrobial Regeneration Consortium as he discusses the development of living nano-biohybrid organisms that use sunlight to convert massive amounts of carbon dioxide.air,and water into biodegradable plastics, fuels, and value-added chemicals suchasferilizers and food-preservatives. During this free Interactive broadcast, he will explain how these "nanobugs" are made by simply combining bacteria found in soil with engineered quantum dots targeted to penetrate the cell, self-assemble on specificenzyme sites, and then trigger these enzymes using different colors of sunlight.</p>

                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="text parbase section">

                                                        <div class="container ">

                                                            <div class="">
                                                                <h2 class="mt-30">What You Will Learn</h2>

                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="text parbase section">
                                                        <div class="container ">

                                                            <div>
                                                                <ul>
                                                                    <li>How to makenewnanobugs by targeting specific enzymes and triggering them with light</li>
                                                                    <li>How to CO2, air, water, and sunlight into a range of fuels, biodegradable plastics, and fertilizers using nanobugs</li>
                                                                    <li>How to engineerquantumdots to selectively attach todesiredenzymes and trigger them with light externally or wirelessly</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="text parbase section">

                                                        <div class="container ">

                                                            <div class="">
                                                                <h2 class="mt-30">Webinar Details</h2>

                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="text parbase section">
                                                        <div class="container ">

                                                            <div>
                                                                <ul>
                                                                    <li>Thursday, November 7, 2019at 2-3 pm ET</li>
                                                                    <li>Fee: Free to Attend</li>
                                                                    <li><a href="#">Download speaker slides</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="text parbase section">

                                                        <div class="container ">

                                                            <div class="">
                                                                <h2 class="mt-30">Additional Resources</h2>

                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="text parbase section">
                                                        <div class="container ">

                                                            <div>
                                                                <ul>
                                                                    <li><a href="">Nanorg Microbial Factories: Light-Driven Renewable Biochemical Synthesis using Quantum Dot-Bacteria Nanobiohybrids </a> Article in JACS by Prashant Nagpal</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="text parbase section">

                                                        <div class="container ">

                                                            <div class="">
                                                                <h4 class="mt-30">Realted Tags:</h4>

                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="text parbase section">
                                                        <div class="container ">

                                                            <div class="related-tag mt-15">
                                                                <ul class="list-inline">
                                                                    <li class="list-inline-item"><a href="#">Lorem ipsum</a></li>
                                                                    <li class="list-inline-item"><a href="#">Phasellus iaculis</a></li>
                                                                    <li class="list-inline-item"><a href="#">Nulla volutpat</a></li>
                                                                    <li class="list-inline-item"><a href="#">Nulla volutpat</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="col-md-4 col-xs-12 columnTwo columnEven">

                                                    <div class="column1 parsys">

                                                        <div class="image parbase section">

                                                            <div class="container">
                                                                <div class="clearfix">

                                                                    <div class="image_left image-div">

                                                                        <!--[if IE 9]></video><![endif]-->

                                                                        <div>
<img src="{{media url=image/acs-webinar-logo.png}}" alt="webinar" class="cq-dd-image"/>

                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="headingtext text parbase section">
                                                            <div class="container">
                                                                <label class="sidebar-label">EXPERTS<br></label>
                                                            </div>
                                                        </div>
                                                        <div class="textimage parbase section">
                                                            <div class="container ">

                                                                <div class="clearfix ">
                                                                    <div class="image_left image-div circle-image">

                                                                        <a href="#">

                                                                            <div class="image">
<img src="{{media url=image/profile-picture.jpg}}" alt="Lena Ruiz Azuara" class="cq-dd-image"/>
                                                                            </div>

                                                                        </a>

                                                                    </div>
                                                                    <div class="text cq-dd-image">

                                                                        <p><small><b><a href="#">Lena Ruiz Azuara</a></b>
<br>
Sociedad Química de México</small></p>
                                                                    </div>

                                                                </div>

                                                            </div>

                                                        </div>

                                                        <div class="textimage parbase section">
                                                            <div class="container ">

                                                                <div class="clearfix ">
                                                                    <div class="image_left image-div circle-image">
                                                                        <a href="#">

                                                                            <!--[if IE 9]></video><![endif]-->

                                                                            <div class="image">
<img src="{{media url=image/profile-picture.jpg}}" alt="Ingrid Montes" class="cq-dd-image"/>
                                                                               

                                                                            </div>

                                                                        </a>

                                                                        <div class="image-info">

                                                                        </div>

                                                                    </div>

                                                                    <div class="text cq-dd-image">

                                                                       <p><small><b><a href="#">Lena Ruiz Azuara</a></b><br>
Universidad de Puerto Rico, Recinto de Río</small></p>

                                                                    </div>

                                                                </div>

                                                            </div>

                                                        </div>

                                                        <div class="headingtext text parbase section">

                                                            <div class="container">

                                                                <h3>Co-produced By
<br>
</h3>

                                                            </div>

                                                        </div>

                                                        <div class="textimage parbase section">

                                                            <div class="container ">

                                                                <div class="clearfix ">

                                                                    <div class="text cq-dd-image">

                                                                        <p><a href="#"><b>Industry Member program</b></a></p>

                                                                    </div>

                                                                </div>

                                                            </div>

                                                        </div>

                                                        <!-- start1 -->
                                                        <div class="headingtext text parbase section">

                                                            <div class="container">

                                                                <h3 class="mt-10" style="color:#333;">Related Topics
<br>
</h3>

                                                            </div>

                                                        </div>
                                                        <div class="textimage parbase section">
                                                            <div class="container ">

                                                                <div class="clearfix ">
                                                                    <div class="image_left image-div">
                                                                        <a href="#">

                                                                            <!--[if IE 9]></video><![endif]-->

                                                                            <div class="image">
<img src="{{media url=image/1566565453833.jpg}}" alt="Ingrid Montes" class="cq-dd-image"/>
                                                                            </div>

                                                                        </a>

                                                                        <div class="image-info">

                                                                        </div>

                                                                    </div>

                                                                    <div class="related-topics-text cq-dd-image">

                                                                        <p><small><a href="#">Innovation Zone <br><span>with Adriana</span></a>
<br>
Universidad de Puerto Rico, Recinto de Río</small></p>

                                                                    </div>

                                                                </div>

                                                            </div>

                                                        </div>
                                                        <!-- end1 -->
                                                        <!-- Start2 -->
                                                        <div class="horizontalrule parbase section">

                                                            <div class="container">

                                                                <hr class="">

                                                            </div>

                                                        </div>
                                                        <div class="textimage parbase section">
                                                            <div class="container ">

                                                                <div class="clearfix ">
                                                                    <div class="image_left image-div">
                                                                        <a href="#">

                                                                            <!--[if IE 9]></video><![endif]-->

                                                                            <div class="image">
<img src="{{media url=image/1566565453833.jpg}}" alt="Ingrid Montes" class="cq-dd-image"/>
                                                                                

                                                                            </div>

                                                                        </a>

                                                                        <div class="image-info">

                                                                        </div>

                                                                    </div>

                                                                    <div class="related-topics-text cq-dd-image">

                                                                        <p><small><a href="#">Treating Diabetes: <br><span>Designing the Once-Weekly & Oral GLP-1 Semaglutide</span></a>
</small></p>

                                                                    </div>

                                                                </div>

                                                            </div>

                                                        </div>

                                                        <!-- end2 -->

                                                        <!-- start3 -->
                                                        <div class="horizontalrule parbase section">

                                                            <div class="container">

                                                                <hr class="">

                                                            </div>

                                                        </div>
                                                        <div class="textimage parbase section">
                                                            <div class="container ">

                                                                <div class="clearfix ">
                                                                    <div class="image_left image-div">
                                                                        <a href="#">

                                                                            <!--[if IE 9]></video><![endif]-->

                                                                            <div class="image">
                                                                                <img src="{{media url=image/1566565453833.jpg}}" alt="Ingrid Montes" class="cq-dd-image"/>
                                                                            </div>

                                                                        </a>

                                                                        <div class="image-info">

                                                                        </div>

                                                                    </div>

                                                                    <div class="related-topics-text cq-dd-image">

                                                                        <p><small><a href="#">Innovation Zone <br><span>with Frank L. Slejko</span></a>
<br>
Investor, Chemical Angel Network</small></p>

                                                                    </div>

                                                                </div>

                                                            </div>

                                                        </div>
                                                        <!-- end3 -->
  <div class="horizontalrule parbase section">

                                                            <div class="container">

                                                                <hr class="">

                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="herocontainer parbase section more-from">
                                <div class="container container-fluid  bottom-promo">
                                    <div class="clearfix text_dark" id="membership-content-mem">
                                        <div class="container">
                                            <div class="containerPar parsys">
                                                <div class="columnsBootstrap parbase section">
                                                    <div class="container">
                                                        <div class="row rowOdd membership-section-5 member-service-section">
                                                            <div class="text cq-dd-image clear">
                                                                <h2 class="mb-30">More from this series</h2>

                                                            </div>
                                                            <div class="col-md-3 col-xs-12 columnOne columnOdd pl-0">
                                                                <div class="column0 parsys">
                                                                    <div class="textimage parbase section">
                                                                        <div class="container ">
                                                                            <div id="_jcrAC_herocontainer_166385261_slash_containerPar_slash_columnsbootstrap_slash_column0_slash_textimage" class="clearfix ">
                                                                                <div class="image_center image-div">
                                                                                    <div class="image">
<img src="{{media url=image/learn-one.jpg}}" alt="Prodrug Strategies in
Medicinal Chemistry" class="cq-dd-image"/>
                                                                                       
                                                                                    </div>
                                                                                    <div class="image-info">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="text cq-dd-image clear">
                                                                                    <h4><b>Prodrug Strategies in
Medicinal Chemistry</b></h4>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 col-xs-12 columnTwo columnEven pl-0">
                                                                <div class="column1 parsys">
                                                                    <div class="textimage parbase section">
                                                                        <div class="container ">
                                                                            <div id="_jcrAC_herocontainer_166385261_slash_containerPar_slash_columnsbootstrap_slash_column1_slash_textimage" class="clearfix ">
                                                                                <div class="image_center image-div">
                                                                                    <a href="#">
                                                                                        <div class="image">
<img src="{{media url=image/learn-two.jpg}}" alt="Prodrug Strategies in
Medicinal Chemistry" class="cq-dd-image"/>
                                                                                            
                                                                                        </div>
                                                                                    </a>
                                                                                    <div class="image-info">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="text cq-dd-image clear">
                                                                                    <h4>Thanksgiving Culinary
Chemistry: Holiday
Rebroadcast</h4>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 col-xs-12 columnThree columnOdd pl-0">
                                                                <div class="column2 parsys">
                                                                    <div class="textimage parbase section">
                                                                        <div class="container ">
                                                                            <div id="_jcrAC_herocontainer_166385261_slash_containerPar_slash_columnsbootstrap_slash_column2_slash_textimage" class="clearfix ">
                                                                                <div class="image_center image-div">
                                                                                    <div class="image">
                                                                                        <img src="{{media url=image/learn-three.jpg}}" alt="Prodrug Strategies in
Medicinal Chemistry" class="cq-dd-image"/>
                                                                                    </div>
                                                                                    <div class="image-info" style="width:275px;">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="text cq-dd-image clear">
                                                                                    <h4>Phase Separation of
Multivalent Proteins: Recent
Findings and New Frontiers</h4>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 col-xs-12 columnFour columnEven pl-0">
                                                                <div class="column3 parsys">
                                                                    <div class="textimage parbase section">
                                                                        <div class="container ">
                                                                            <div id="_jcrAC_herocontainer_166385261_slash_containerPar_slash_columnsbootstrap_slash_column3_slash_textimage" class="clearfix ">
                                                                                <div class="image_center image-div">
                                                                                    <a href="#">
                                                                                        <div class="image">
                                                                                            <img src="{{media url=image/learn-four.jpg}}" alt="Prodrug Strategies in
Medicinal Chemistry" class="cq-dd-image"/>
                                                                                        </div>
                                                                                    </a>
                                                                                    <div class="image-info">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="text cq-dd-image clear">
                                                                                    <h4>The Chemistry of Death:
Halloween Rebroadcast</h4>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /row-fluid -->
        </div>

    </div>

    <!--====== end  page content ====== -->
<div id="custom_popup" style="display:none;">
    <div class="youtube-video">
        <img src="{{media url=image/login.jpg}}" alt="" width="100%" height="350px" />
    </div>
    <fieldset class="fieldset">
        <div class="field">
  
            <div class="control">
                <input type="checkbox" name="checkbox" id="checkbox" class="checkbox" title=""> Accept Login
            </div>
        </div>
    </fieldset>
</div>
<script>
    require(
        [
            'jquery',
            'Magento_Ui/js/modal/modal'
        ],
        function(
            $,
            modal
        ) {
            var options = {
                type: 'popup',
                responsive: true,
                innerScroll: false,
                buttons: [{
                    text: $.mage.__('OK'),
                    class: '',
                    click: function () {
                        if($("#checkbox" ).is(':checked')){
                          window.location='{{config path="web/unsecure/base_url"}}/webinar-registration-form';
                        } else{
                            alert("Please agree to the terms and conditions");

                        }
                    }
                }]
            };

            var popup = modal(options, $('#custom_popup'));
            $("#reg1").on('click',function(){ 
                $("#custom_popup").modal("openModal");
            });

        }
    );
</script>