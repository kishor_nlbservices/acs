<?php 

namespace Acstechnical\Division\Block\Division;
use Magento\Framework\View\Element\Template;
class Index extends \Magento\Framework\View\Element\Template
{
	public function __construct(\Magento\Framework\View\Element\Template\Context $context)
	{
		parent::__construct($context);
	}

	public function sayHello()
	{
		return __('Hello World');
	}
	
	public function _prepareLayout()
	{
	$this->pageConfig->getTitle()->set(__('Hello Us')); // add title here
	}

}
?>