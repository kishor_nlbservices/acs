<?php
namespace Pitbulk\SAML2\Observer;

use Magento\Backend\Model\Auth\Session;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class AdminLoginObserverStoreSession implements ObserverInterface
{
    /**
     * @var Session
     */
    private $session;

    public function __construct(
        Session $session
    ) {
        $this->session = $session;
    }

    public function execute(Observer $observer)
    {
        $event = $observer->getEvent();
        $user = $event->getUser();

        $samlLogin = $user->getSamlLogin();
        $nameId = $user->getSamlNameId();
        $nameIdFormat = $user->getSamlNameIdFormat();
        $sessionIndex = $user->getSamlSessionIndex();
        if (!empty($samlLogin)) {
            $this->session->setData('backend_saml_login', true);
        }
        if (!empty($nameId)) {
            $this->session->setData('backend_saml_nameid', $nameId);
        }
        if (!empty($nameIdFormat)) {
            $this->session->setData('backend_saml_nameid_format', $nameIdFormat);
        }
        if (!empty($sessionIndex)) {
            $this->session->setData('backend_saml_sessionindex', $sessionIndex);
        }
    }
}
