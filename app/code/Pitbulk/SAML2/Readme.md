Single Sign On. SAML extension for Magento 2. (frontend + backend) Implemented by Sixto Martin
==============================================================================================

Description
-----------

Magento2 extension that adds SAML Single Sign On support to the customer login page or/and to the backend login page.


If you are working with a partner that has implemented a SAML identity provider, you can use this extension to interoperate with it, thereby enabling SSO for customers. It works with any IDP providers, including OneLogin, Okta, Ping Identity, ADFS, Salesforce, SharePoint...

The module was implemented by [Sixto Martin](http://saml.info) , author of 15+ SAML plugins and several SAML toolkits.

The module was implemented for Magento 2, If you are interested in a SAML module compatible with Magento 1.X search on the Magento marketplace for a Magento1 version.

Customers are happy with the SAML extension I made and with the support received. Companies like Cisco, Toyota or PWC trusted in the SAML extensions.



How does it work?
-----------------

The normal usage
................

Extension adds a link "Login via Identity provider" at the customer login form or/and to the backend login page.
Following this links initiates series of redirects that are described by [SAML 2.0 standart](http://en.wikipedia.org/wiki/SAML_2.0)

Customer authenticates against the SAML Identity Provider and then information about user, group and address is sent to Magento. Magento authenticate customer and let him in. 
Similar happens with users on the backend.

Other usages
............

Extension supports IdP-Initiated so a SAML Response can be directly processed by the Magento instance.



Features
--------

* Allow to Login via any Identity Provider.
* Easily switch On/Off the SAML Module.
* Supports Single Sign On (IdP and SP initiated)
* Supports Single Log Out (IdP and SP initiated)
* Supports Just-In-Time Provisioning (customer data + group + address // user data + role)
* Possibly set the mapping between IdP fields and Magento fields.
* Customizable workflow.
* Supports Magento Multi-stores.
* Documented settings



Installation
------------

Review the Installation.md file or the "Installation guide" documentation.



Settings
--------

The Settings of the extension are available at Stores > Configuration. At the Services tab:
* "SAML SSO for customers" link belong the SAML settings for the frontend.
* "SAML SSO for admins (backend)" link belong the SAML settings for the backend.

There you will be able to fill several sections:

 * Status. To enable or disable the extension. 
 * Identity Provider. Set parameters related to the IdP that will be connected with our Magento.
 * Options. The behavior of the extension. 
 * Attribute Mapping. Set the mapping between IdP fields and Magento user fields.
 * Group / Role Mapping. Set the mapping between IdP groups and Magento groups (IdP roles and Magento role, in case of backend settings).
 * Address Mapping. Set the mapping between IdP fields and Magento address fields (only on frontend settings).
 * Custom messages. To handle what messages are showed in the login form. 
 * Advanced settings. Handle some other parameters related to customizations and security issues.


The metadata of the Magento Service Provider will be available at:
* http://<magento_base_url>/sso/saml/metadata         (frontend)
* http://<magento_base_url>/sso/saml/backendmetadata  (backend)

At the Status section you are asked for a license key. Use the Order ID of your magento marketplace’s purchase.

Warranty
--------

Support by mail <sixto.martin.garcia@gmail.com> guaranteed. Get a reply in less than 48h (business day).


License warning
---------------

When you purchase the extension, you are able to use it at one M2 instance.

In case of M2 running multi-sites, the license cover 3 stores using SAML SSO. If you require more stores, contact sixto.martin.garcia@gmail.com to discuss the terms.

Test and developer environments can use the extension without require an additional license