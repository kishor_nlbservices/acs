<?php
/**
 * SAML Extension for Magento2.
 *
 * @package     Pitbulk_SAML2
 * @copyright   Copyright (c) 2019 Sixto Martin Garcia (http://saml.info)
 * @license     Commercial
 */

namespace Pitbulk\SAML2\Controller\Adminhtml\Auth;

use Magento\Backend\App\Action\Context;
use Pitbulk\SAML2\Helper\Data;

class Logout extends \Magento\Backend\Controller\Adminhtml\Auth\Logout
{
    /**
     * @var \Pitbulk\SAML2\Helper\Data
     */
    private $helper;

    public function __construct(
        Context $context,
        Data $helper
    ) {
        $this->helper = $helper;
        parent::__construct($context);
    }

    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        
        $skipNormalLogout = false;

        $moduleEnabled = $this->helper->checkEnabledModule('backend');

        if ($moduleEnabled) {
            $sloParm = 'pitbulk_saml2_admin/options/slo';
            $sloEnabled = $this->helper->getConfig($sloParm);
            if ($sloEnabled &&
              $this->_auth->getAuthStorage()->getData('backend_saml_login') &&
              $this->_auth->isLoggedIn()) {
                $skipNormalLogout = true;
                // redirect to /admin/sso/saml2/logout
                $sloUrl = $this->helper->getBackendUrl('/sso/saml2/logout');
                $resultRedirect->setUrl($sloUrl);
            }
        }

        if (!$skipNormalLogout) {
            $resultRedirect = parent::execute();
        }

        return $resultRedirect;
    }
}
