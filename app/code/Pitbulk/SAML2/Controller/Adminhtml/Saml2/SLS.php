<?php
/**
 * SAML Extension for Magento2.
 *
 * @package     Pitbulk_SAML2
 * @copyright   Copyright (c) 2019 Sixto Martin Garcia (http://saml.info)
 * @license     Commercial
 */

namespace Pitbulk\SAML2\Controller\Adminhtml\Saml2;

use Magento\Backend\App\Action;
use Magento\Backend\Model\Auth\Session;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\UrlInterface as BackendUrl;

use Pitbulk\SAML2\Helper\Data;

class SLS extends Action
{
    /**
     * @var Data
     */
    public $helper;
    /**
     * @var Session
     */
    public $session;

    public function __construct(
        Context $context,
        Session $session,
        Data $helper
    ) {
        $this->_publicActions = ['sls'];
        $this->helper = $helper;
        $this->session = $session;

        parent::__construct($context);

        $this->_whitelistEndpoint();
    }

    public function _whitelistEndpoint()
    {
        // CSRF Magento2.3 compatibility
        if (interface_exists("\Magento\Framework\App\CsrfAwareActionInterface")) {
            $request = $this->getRequest();
            if ($request instanceof HttpRequest && empty($request->getParam(BackendUrl::SECRET_KEY_PARAM_NAME))) {
                $request->setParam(BackendUrl::SECRET_KEY_PARAM_NAME, $this->_backendUrl->getSecretKey());
            }
        }
    }

    public function execute()
    {
        $errorMsg = null;

        $moduleEnabled = $this->helper->checkEnabledModule('backend');
        if (!$moduleEnabled) {
            $errorMsg = "The backend SAML2 module has disabled status";
            $this->_processError($errorMsg);
            return;
        }
        
        $sloEnabled = $this->helper->getConfig('pitbulk_saml2_admin/options/slo');
        if (!$sloEnabled) {
            $errorMsg = "The backend SAML2 SLO is disabled";
            $this->_processError($errorMsg);
            return;
        }

        if (!$this->session->isLoggedIn()) {
            $errorMsg = "You tried to execute a backend SAML logout " .
                            "process but you are not logged via SAML";
            $this->_processError($errorMsg);
            return;
        }
            
        $samlLogin = $this->session->getData('backend_saml_login');
            
        if ($samlLogin) {
            $auth = $this->helper->getAuth('backend');
            
            $auth->processSLO();
            $errors = $auth->getErrors();
            if (empty($errors)) {
                // local logout
                $this->session->processLogout();
                return $this->_redirect($this->_backendUrl->getBaseUrl());
            } else {
                $errorMsg = 'Error at the Backend SLS Endpoint.<br>' .
                            implode(', ', $errors);
                if ($helper->getConfig('pitbulk_saml2_admin/advanced/debug')) {
                    $reason = $auth->getLastErrorReason();
                    if (isset($reason) && !empty($reason)) {
                        $errorMsg .= '<br><br>Reason: '.$reason;
                    }
                }
            }
        } else {
            $errorMsg = "You tried to execute a backend SAML logout " .
                        "process but you are not logged via SAML";
        }

        if (isset($errorMsg)) {
            $this->_processError($errorMsg);
        }
    }

    private function _processError($errorMsg)
    {
        $this->messageManager->addErrorMessage($errorMsg);
        return $this->_redirect($this->_backendUrl->getStartupPageUrl());
    }
}
